// Modules
import { RouterModule, Routes } from '@angular/router';

// Components
import {PagesComponent} from './pages/pages.component';
import {LoginComponent} from './login/login.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    loadChildren: './pages/pages.module#PagesModule'
  },
  // { path: '**', component: NopagefoundComponent }
  {path: 'login', component: LoginComponent},
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes );
