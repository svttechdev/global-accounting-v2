import { Component } from '@angular/core';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'global-accounting';
  constructor(public afService: AuthenticationService) {

    this.afService.fnInitializeFirebase();

    this.afService.fnCheckAuth();

    localStorage.removeItem('firebase:previous_websocket_failure');

  }
}
