import {IsNumber, IsString, Min, MinLength} from "class-validator";

export class Expenses {
  @MinLength(1, {
    groups: ['registration']
  })
  key_major_subject: string;
  @MinLength(1, {
    groups: ['registration']
  })
  key_minor_subject: string;
  suppliers: string;
  remarks: string;
  localCurrency: number;
  @Min( 1, {
    groups: ['registration']
  })
  jpy: number;
  @MinLength(1, {
    groups: ['registration']
  })
  key_bank_account: string;
  @MinLength(1, {
    groups: ['registration']
  })
  key_contact: string;
  @Min( 1, {
    groups: ['registration']
  })
  withdrawalDate: any;

  constructor() {
    this.key_major_subject = '';
    this.key_minor_subject = '';
    this.jpy = 0;
    this.key_bank_account = '';
    this.key_contact = '';
    this.withdrawalDate = 0;
  }

}
