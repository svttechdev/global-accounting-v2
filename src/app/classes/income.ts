import {IsNumber, IsString, Min, MinLength} from 'class-validator';

export class Income {
  @MinLength(1, {
    groups: ['registration']
  })
  key_major_subject: string;
  @MinLength(1, {
    groups: ['registration']
  })
  key_minor_subject: string;
  suppliers: string;
  remark: string;
  localCurrency: number;
  @Min( 1, {
    groups: ['registration']
  })
  jpy: number;
  @MinLength(1, {
    groups: ['registration']
  })
  key_bank_account: string;
  @MinLength(1, {
    groups: ['registration']
  })
  key_contact: string;
  @Min( 1, {
    groups: ['registration']
  })
  paymentDate: any;
}
