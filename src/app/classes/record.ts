import {Expenses} from './expenses';
import {Income} from './income';
import {Balance} from './balance';

export class Record {
  date: any;
  expense: Expenses;
  income: Income;
  balance: Balance;
  constructor() {

  }
}
