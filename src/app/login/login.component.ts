import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../services/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private afService:  AuthenticationService) { }

  ngOnInit() {
  }

  fnLoginWithGoogle() {
    this.afService.loginWithGoogle();
  }

}
