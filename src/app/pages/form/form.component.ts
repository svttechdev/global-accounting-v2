import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {MajorSubject} from '../../classes/major-subject';
import {Record} from '../../classes/record';
import {Expenses} from '../../classes/expenses';
import {Income} from '../../classes/income';
import {Balance} from '../../classes/balance';
import {format, addDays, endOfDay} from 'date-fns';
import {validate} from 'class-validator';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  propertyId: string;
  mMajorSubject: MajorSubject;
  mRecord: Record;
  mDate: any;
  mDate2: any;
  mDate3: any;
  mRefUid: any;

  mArrayCategoriesExpenses: Array<any>;
  mArrayCategoriesIncome: Array<any>;

  mArraySubcategoriesExpenses: Array<any>;
  mArraySubcategoriesIncome: Array<any>;

  mArrayBankAccount: Array<any>;
  mArrayContact: Array<any>;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
    route.params.subscribe(params => {
      this.propertyId = params['id'];
      this.afService.mSelectedPropertyID = this.propertyId;
      const mUidRecord = params.action;
      if (mUidRecord !== 'new') {
        this.mRefUid = this.route.snapshot.paramMap.get('action');
        this.fnGetRecord().then(() => {
          if (this.mRecord.income && this.mRecord.income.key_major_subject) {
            this.fnEventSelectedIncome(this.mRecord.income.key_major_subject);
          }
          if (this.mRecord.expense && this.mRecord.expense.key_major_subject) {
            this.fnEventSelectedExpenses(this.mRecord.expense.key_major_subject);
          }

        });
      }
    });
  }

  ngOnInit() {
    this.mMajorSubject = new MajorSubject();
    this.mRecord = new Record();
    this.mRecord.expense = new Expenses();
    this.mRecord.income = new Income();
    this.mRecord.balance = new Balance();

    this.mDate = format(endOfDay(new Date()), 'yyyy-MM-dd');

    this.afService.db
      .collection('major_subject')
      .onSnapshot(snapshot => {

        this.mArrayCategoriesExpenses = new Array<any>();
        this.mArrayCategoriesIncome = new Array<any>();

        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;

          if (mCategory.type === 'expenses') {
            this.mArrayCategoriesExpenses.push(mCategory);
          } else if (mCategory.type === 'income') {
            this.mArrayCategoriesIncome.push(mCategory);
          }


        });
        // console.log(this.mArrayCategoriesExpenses);

      });
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('bank_account')
      .onSnapshot(snapshot => {

        this.mArrayBankAccount = new Array<any>();

        snapshot.forEach(snap => {
          const mBankAccount = snap.data();
          mBankAccount.id = snap.id;

          this.mArrayBankAccount.push(mBankAccount);

        });
        // console.log(this.mArrayBankAccount);

      });
    this.afService.db
      .collection('contact')
      .onSnapshot(snapshot => {

        this.mArrayContact = new Array<any>();

        snapshot.forEach(snap => {
          const mContact = snap.data();
          mContact.id = snap.id;

          this.mArrayContact.push(mContact);

        });

      });
  }

  fnEventSelectedIncome($event) {
    console.log(this.mMajorSubject);
    this.afService.db
      .collection('major_subject')
      .doc($event)
      .collection('minor_subject')
      .onSnapshot(snapshot => {
        this.mArraySubcategoriesIncome = new Array<any>();
        snapshot.forEach(snap => {
          const mSubcategory = snap.data();
          mSubcategory.id = snap.id;
          this.mArraySubcategoriesIncome.push(mSubcategory);
        });
        console.log(this.mArraySubcategoriesIncome);
      });
    return $event;
  }

  fnEventSelectedExpenses($event) {
    console.log(this.mMajorSubject);
    this.afService.db
      .collection('major_subject')
      .doc($event)
      .collection('minor_subject')
      .onSnapshot(snapshot => {
        this.mArraySubcategoriesExpenses = new Array<any>();
        snapshot.forEach(snap => {
          const mSubcategory = snap.data();
          mSubcategory.id = snap.id;
          this.mArraySubcategoriesExpenses.push(mSubcategory);
        });
      });
    return $event;
  }

  fnSaveExpenses() {

    this.mDate = addDays(endOfDay(new Date(this.mDate)), 1);
    this.mDate2 = addDays(endOfDay(new Date(this.mDate2)), 1);
    this.mDate3 = addDays(endOfDay(new Date(this.mDate3)), 1);

    this.mRecord.date = this.afService.fnCreateDate(
      new Date(this.mDate)
    );
    this.mRecord.expense.withdrawalDate = +new Date(this.mDate2);
    // this.mRecord.income.paymentDate = +new Date(this.mDate3);

    const mTmpObj = Object.assign({}, this.mRecord);
    mTmpObj.expense = Object.assign({}, mTmpObj.expense);
    mTmpObj.income = Object.assign({}, mTmpObj.income);
    mTmpObj.balance = Object.assign({}, mTmpObj.balance);

    const mExpense = Object.assign(new Expenses(), this.mRecord.expense);

    validate(mExpense, {
      groups: ['registration']
    }).then(errors1 => {
      console.log(errors1);
      if (errors1.length > 0) {
        console.log('not ok');
      } else {
        // mTmpObj.income.paymentDate = this.afService.fnCreateDate(
        //   new Date(mTmpObj.income.paymentDate)
        // );
        mExpense.withdrawalDate = this.afService.fnCreateDate(
          mExpense.withdrawalDate
        );
        if (this.mRefUid) {
          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('record')
            .doc(this.mRefUid)
            .update(
              {
                date: this.mRecord.date,
                expense: Object.assign({}, mExpense)
              }
            )
            .then(() => {
              console.log('Document successfully updated!');
              console.log('Document successfully updated!' + this.mRefUid);
              window.location.href = '/' + 'property-record' + '/' + this.afService.mSelectedPropertyID + '/' + 'daily';
            })
            .catch((error) => {
              console.error('Error updating document: ', error);
            });
        } else {
          mTmpObj.expense = Object.assign({}, mExpense);
          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('record')
            .add(
              mTmpObj
            )
            .then(docRef => {
              console.log('Document successfully written!');
              console.log('Document successfully updated!' + docRef.id);
              this.afService.fnRedirect('property-record/' + this.afService.mSelectedPropertyID + '/daily');
            });
        }
      }
    });
  }

  fnSaveIncome() {

    this.mDate = addDays(endOfDay(new Date(this.mDate)), 1);
    this.mDate2 = addDays(endOfDay(new Date(this.mDate2)), 1);
    this.mDate3 = addDays(endOfDay(new Date(this.mDate3)), 1);

    this.mRecord.date = this.afService.fnCreateDate(
      new Date(this.mDate)
    );
    this.mRecord.expense.withdrawalDate = +new Date(this.mDate2);
    this.mRecord.income.paymentDate = +new Date(this.mDate3);

    const mTmpObj = Object.assign({}, this.mRecord);
    mTmpObj.expense = Object.assign({}, mTmpObj.expense);
    mTmpObj.income = Object.assign({}, mTmpObj.income);
    mTmpObj.balance = Object.assign({}, mTmpObj.balance);


    const mIncome = Object.assign(new Income(), this.mRecord.income);

    validate(mIncome, {
      groups: ['registration']
    }).then(errors1 => {
      console.log(errors1);
      if (errors1.length > 0) {
        console.log('not ok');
      } else {
        mIncome.paymentDate = this.afService.fnCreateDate(
          new Date(mIncome.paymentDate)
        );

        if (this.mRefUid) {
          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('record')
            .doc(this.mRefUid)
            .update(
              {
                date: this.mRecord.date,
                income: Object.assign({}, mIncome)
              }
            )
            .then(() => {
              console.log('Document successfully updated!');
              window.location.href = '/' + 'property-record' + '/' + this.afService.mSelectedPropertyID + '/' + 'daily';
            })
            .catch((error) => {
              console.error('Error updating document: ', error);
            });
        } else {
          mTmpObj.income = Object.assign({}, mIncome);
          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('record')
            .add(
              mTmpObj
            )
            .then(docRef => {
              console.log('Document successfully written!');
              this.afService.fnRedirect('property-record/' + this.afService.mSelectedPropertyID + '/daily');
            });
        }
      }
    });
  }

  fnSaveRecord() {
    this.mDate = addDays(endOfDay(new Date(this.mDate)), 1);
    this.mDate2 = addDays(endOfDay(new Date(this.mDate2)), 1);
    this.mDate3 = addDays(endOfDay(new Date(this.mDate3)), 1);

    this.mRecord.date = this.afService.fnCreateDate(
      new Date(this.mDate)
    );
    this.mRecord.expense.withdrawalDate = +new Date(this.mDate2);
    this.mRecord.income.paymentDate = +new Date(this.mDate3);

    const mTmpObj = Object.assign({}, this.mRecord);
    mTmpObj.expense = Object.assign({}, mTmpObj.expense);
    mTmpObj.income = Object.assign({}, mTmpObj.income);
    mTmpObj.balance = Object.assign({}, mTmpObj.balance);


    const mIncome = Object.assign(new Income(), this.mRecord.income);
    const mExpense = Object.assign(new Expenses(), this.mRecord.expense);

    validate(mExpense, {
      groups: ['registration']
    }).then(errors => {
      if (errors.length > 0) {
        console.log('not ok');
      } else {
        validate(mIncome, {
          groups: ['registration']
        }).then(errors1 => {
          if (errors1.length > 0) {
            console.log('not ok');
          } else {
            mExpense.withdrawalDate = this.afService.fnCreateDate(
              new Date(mExpense.withdrawalDate)
            );
            mIncome.paymentDate = this.afService.fnCreateDate(
              new Date(mIncome.paymentDate)
            );
            this.afService.db
              .collection('properties')
              .doc(this.propertyId)
              .collection('record')
              .add(
                mTmpObj
              )
              .then(docRef => {
                console.log('Document successfully written!');
                this.afService.fnRedirect('property-record/' + this.afService.mSelectedPropertyID + '/daily');
              });
          }
        });
      }
    });

  }

  fnGetRecord() {
    return new Promise(((resolve, reject) => {
      this.afService.db
        .collection('properties')
        .doc(this.propertyId)
        .collection('record')
        .doc(this.mRefUid)
        .get()
        .then(docRef => {
          if (docRef.exists) {
            this.mRecord = docRef.data();
            console.log(this.mRecord);

            if (this.mRecord.date) {
              this.mRecord.date = this.mRecord.date.toDate();
            }
            if (this.mRecord.expense.withdrawalDate) {
              this.mRecord.expense.withdrawalDate = this.mRecord.expense.withdrawalDate.toDate();
            }
            if (this.mRecord.income.paymentDate) {
              this.mRecord.income.paymentDate = this.mRecord.income.paymentDate.toDate();
            }

            this.mDate = format(new Date(this.mRecord.date), 'yyyy-MM-dd');
            if (!isNaN(this.mRecord.expense.withdrawalDate)) {
              this.mDate2 = format(new Date(this.mRecord.expense.withdrawalDate), 'yyyy-MM-dd');
            }
            if (!isNaN(this.mRecord.income.paymentDate)) {
              this.mDate3 = format(new Date(this.mRecord.income.paymentDate), 'yyyy-MM-dd');
            }
            resolve(this.mRecord);
          } else {
            reject();
          }
        });
    }));
  }

  fnUpdate() {
    this.mDate = addDays(endOfDay(new Date(this.mDate)), 1);
    this.mDate2 = addDays(endOfDay(new Date(this.mDate2)), 1);
    this.mDate3 = addDays(endOfDay(new Date(this.mDate3)), 1);

    this.mRecord.date = +new Date(this.mDate);
    this.mRecord.expense.withdrawalDate = +new Date(this.mDate2);
    this.mRecord.income.paymentDate = +new Date(this.mDate3);
    const mTmpObj = Object.assign({}, this.mRecord);
    mTmpObj.expense = Object.assign({}, mTmpObj.expense);
    mTmpObj.income = Object.assign({}, mTmpObj.income);
    mTmpObj.balance = Object.assign({}, mTmpObj.balance);

    const mIncome = Object.assign(new Income(), this.mRecord.income);
    const mExpense = Object.assign(new Expenses(), this.mRecord.expense);

    validate(mExpense, {
      groups: ['registration']
    }).then(errors => {
      if (errors.length > 0) {
        console.log('not ok');
      } else {
        validate(mIncome, {
          groups: ['registration']
        }).then(errors1 => {
          if (errors1.length > 0) {
            console.log('not ok');
          } else {
            mTmpObj.expense.withdrawalDate = this.afService.fnCreateDate(
              new Date(mExpense.withdrawalDate)
            );
            mTmpObj.income.paymentDate = this.afService.fnCreateDate(
              new Date(mIncome.paymentDate)
            );
            this.afService.db
              .collection('properties')
              .doc(this.propertyId)
              .collection('record')
              .doc(this.mRefUid)
              .update(
                Object.assign({}, mTmpObj)
              )
              .then(() => {
                console.log('Document successfully updated!');
                window.location.href = '/' + 'property-record' + '/' + this.afService.mSelectedPropertyID + '/' + 'daily';
              })
              .catch((error) => {
                console.error('Error updating document: ', error);
              });
          }
        });
      }
    });

  }

}
