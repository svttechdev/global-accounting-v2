import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";
import {ActivatedRoute} from "@angular/router";
import {endOfDay, endOfMonth, startOfMonth} from "date-fns";
import * as crossfilter from "crossfilter";
import * as dc from "dc";
import * as d3Scale from "d3-scale";
import * as d3 from "d3";

@Component({
  selector: 'app-global-dashboard',
  templateUrl: './global-dashboard.component.html',
  styleUrls: ['./global-dashboard.component.css']
})
export class GlobalDashboardComponent implements OnInit {
  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mArrayRecords: Array<any>;
  mArrayMajorSubjects: Array<any>;
  mObjectMajorSubjects: any;

  mArrayProperties: Array<any>;

  innerWidth: number;

  constructor(private afService: AuthenticationService) {
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }


    this.fnPromiseGetCategories()
      .then(result => {
        console.log(result);
      });

    this.fnPromiseGetAllProperties()
      .then(result => {
        console.log(result);
      });
  }

  fnPromiseGetCategories() {
    return new Promise(resolve => {
      this.afService.db
        .collection('major_subject')
        .get()
        .then(snapshot => {
          this.mArrayMajorSubjects = new Array<any>();
          this.mObjectMajorSubjects = {};
          const mTmpObj = {};
          snapshot.forEach(majorSubject => {
            const mMajorsubject = majorSubject.data();
            mMajorsubject.id = majorSubject.id;
            mTmpObj[mMajorsubject.id] = mMajorsubject;
            this.mArrayMajorSubjects.push(mMajorsubject);
          });
          this.mObjectMajorSubjects = mTmpObj;
          resolve(true);
        });
    });
  }

  fnPromiseGetAllProperties() {
    return new Promise(result => {
      this.afService.db
        .collection('properties')
        .get()
        .then(snapProperties => {
          this.mArrayProperties = new Array<any>();
          snapProperties.forEach(propertySnap => {
            const mProperty = propertySnap.data();
            mProperty.id = propertySnap.id;
            this.mArrayProperties.push(mProperty);
          });
          result(true);
        });
    });
  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      // this.fnGetRecords(mNewDate);
      this.mArrayRecords = new Array<any>();
      Promise.all(
        this.mArrayProperties.map(mProperty => {
          console.log(mProperty);
          return this.fnGetRecords(mNewDate, mProperty);
        })
      ).then(results => {
      }).catch(errpr => {
      });

    }
  }

  fnGetRecords(date: Date, mProperty) {
    return new Promise((result, reject) => {
      this.afService.db
        .collection('properties')
        .doc(mProperty.id)
        .collection('record')
        .where('date', '>=', +startOfMonth(date))
        .where('date', '<=', +endOfMonth(date))
        .get()
        .then(snapshot => {

          snapshot.forEach(record => {
            // jpy
            // key_bank_account
            // key_contact
            // key_major_subject
            // key_minor_subject
            // localCurrency
            // withdrawalDate
            const mRecord = record.data();
            mRecord.id = record.id;
            mRecord.date = +endOfDay(mRecord.date);

            if (mRecord.expense &&
              mRecord.expense.jpy && mRecord.expense.localCurrency &&
              mRecord.expense.key_major_subject && mRecord.expense.key_minor_subject &&
              mRecord.expense.key_bank_account) {
              mRecord.expense.date = mRecord.date;
              mRecord.expense.key = 'expense';
              mRecord.expense.key_property = mProperty.name;

              mRecord.expense.key_major_subject = this.mObjectMajorSubjects[mRecord.expense.key_major_subject].name;
              this.mArrayRecords.push(
                mRecord.expense
              );
            }

            if (mRecord.income &&
              mRecord.income.jpy && mRecord.income.localCurrency &&
              mRecord.income.key_major_subject && mRecord.income.key_minor_subject &&
              mRecord.income.key_bank_account) {
              mRecord.income.date = mRecord.date;
              mRecord.income.key = 'income';
              mRecord.income.key_property = mProperty.name;

              mRecord.income.key_major_subject = this.mObjectMajorSubjects[mRecord.income.key_major_subject].name;
              this.mArrayRecords.push(
                mRecord.income
              );
            }

          });
          console.log(this.mArrayRecords);
          this.fnInitGraphs();
          result(true);
        })
        .catch(() => {
          reject(false);
        });
    });

  }

  fnInitGraphs() {

    const ndx = crossfilter(this.mArrayRecords);

    const dateDimension = ndx.dimension((d) => {
      return d.date;
    });
    const maxDate = dateDimension.top(1)[0]['date'];
    const minDate = dateDimension.bottom(1)[0]['date'];


    this.render_graph_composite('incomes-expenses-chart', dateDimension, minDate, maxDate);
    this.render_major_categories('major-categories-chart', ndx);

    this.render_graph_composite_incomes('incomes-chart', dateDimension, minDate, maxDate);
    this.render_graph_composite_expenses('expenses-chart', dateDimension, minDate, maxDate);

    this.render_graph_composite_incomes_property('property-incomes-chart', dateDimension, minDate, maxDate);
    this.render_graph_composite_expenses_property('property-expenses-chart', dateDimension, minDate, maxDate);

    dc.renderAll();
  }

  render_graph_composite_incomes_property(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 30, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayProperties.forEach(mSubj => {

      const incomes = dateDimension.group().reduce(
        (p, v) => {
          p.total += (v['key_property'] === mSubj.name && v['key'] === 'income' ? v['jpy'] : 0);
          return p;
        },
        (p, v) => {
          p.total -= (v['key_property'] === mSubj.name && v['key'] === 'income' ? v['jpy'] : 0);
          return p;
        },
        () => {
          return {total: 0};
        }
      );

      mTmpArray.push(
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[mTmpArray.length])
          .group(incomes, mSubj.name)
          .valueAccessor(d => {
            return d.value.total;
          })
      );

    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite_expenses_property(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 30, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayProperties.forEach(mSubj => {

      const incomes = dateDimension.group().reduce(
        (p, v) => {
          p.total += (v['key_property'] === mSubj.name && v['key'] === 'expense' ? v['jpy'] : 0);
          return p;
        },
        (p, v) => {
          p.total -= (v['key_property'] === mSubj.name && v['key'] === 'expense' ? v['jpy'] : 0);
          return p;
        },
        () => {
          return {total: 0};
        }
      );

      mTmpArray.push(
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[mTmpArray.length])
          .group(incomes, mSubj.name)
          .valueAccessor(d => {
            return d.value.total;
          })
      );

    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite_incomes(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 30, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayMajorSubjects.forEach(mSubj => {
      if (mSubj.type === 'income') {
        const incomes = dateDimension.group().reduce(
          (p, v) => {
            p.total += (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          (p, v) => {
            p.total -= (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          () => {
            return {total: 0};
          }
        );
        mTmpArray.push(
          dc.lineChart(compositeChart)
            .dimension(dateDimension)
            .colors(d3.schemeSet1[mTmpArray.length])
            .group(incomes, mSubj.name)
            .valueAccessor(d => {
              return d.value.total;
            })
        );
      }
    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite_expenses(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 30, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayMajorSubjects.forEach(mSubj => {
      if (mSubj.type === 'expenses') {
        const incomes = dateDimension.group().reduce(
          (p, v) => {
            p.total += (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          (p, v) => {
            p.total -= (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          () => {
            return {total: 0};
          }
        );
        mTmpArray.push(
          dc.lineChart(compositeChart)
            .dimension(dateDimension)
            .colors(d3.schemeSet1[mTmpArray.length])
            .group(incomes, mSubj.name)
            .valueAccessor(d => {
              return d.value.total;
            })
        );
      }
    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite(idDiv: string, dateDimension, minDate, maxDate) {

    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const incomes = dateDimension.group().reduce(
      (p, v) => {
        p.total += (v['key'] === 'income' ? v['jpy'] : 0);
        return p;
      },
      (p, v) => {
        p.total -= (v['key'] === 'income' ? v['jpy'] : 0);
        return p;
      },
      () => {
        return {total: 0};
      }
    );

    const expenses = dateDimension.group().reduce(
      (p, v) => {
        p.total += (v['key'] === 'expense' ? v['jpy'] : 0);
        return p;
      },
      (p, v) => {
        p.total -= (v['key'] === 'expense' ? v['jpy'] : 0);
        return p;
      },
      () => {
        return {total: 0};
      }
    );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 30, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(65).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose([
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[0])
          .group(incomes, 'Incomes')
          .valueAccessor(d => {
            return d.value.total;
          }),
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[1])
          .group(expenses, 'Expenses')
          .valueAccessor(d => {
            return d.value.total;
          })
      ])
      .brushOn(false);

  }

  render_major_categories(idDiv: string, ndx) {
    const runDimension = ndx.dimension(d => {
      return d['key_major_subject'];
    });
    const speedSumGroup = runDimension.group().reduceSum(d => {
      return +d['jpy'];
    });

    const chart = dc.pieChart('#' + idDiv);
    chart
      .width(this.innerWidth / 3)
      .height(this.innerWidth / 3)
      .innerRadius(100)
      .dimension(runDimension)
      .group(speedSumGroup)
      .legend(dc.legend());
  }


}
