import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.css']
})
export class GoalsComponent implements OnInit {
  mArrayMonths = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mObjCategories: any;
  mArrayIncomeCategories: Array<any>;
  mArrayExpensesCategories: Array<any>;
  mArrayProperties: Array<any>;

  constructor(private afService: AuthenticationService) {
  }

  ngOnInit() {
    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }
    this.fnGetCategories();
  }

  fnGetCategories() {
    this.afService.db
      .collection('major_subject')
      .get()
      .then(snapshot => {
        this.mArrayIncomeCategories = new Array<any>();
        this.mArrayExpensesCategories = new Array<any>();

        const tmpEmpty = {};
        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;
          tmpEmpty [mCategory.id] = mCategory;
          if (mCategory.type === 'income') {
            this.mArrayIncomeCategories.push(tmpEmpty [mCategory.id]);
          } else if (mCategory.type === 'expenses') {
            this.mArrayExpensesCategories.push(tmpEmpty [mCategory.id]);
          }
        });

        this.mObjCategories = tmpEmpty;
        this.fnGetProperties();
        console.log(this.mArrayIncomeCategories);
      });
  }

  fnGetProperties() {
    this.afService.db
      .collection('properties')
      .orderBy('name', 'asc')
      .get()
      .then(snapshotProperties => {
        this.mArrayProperties = new Array<any>();

        snapshotProperties.forEach(snapProperty => {
          const mProperty = snapProperty.data();
          mProperty.id = snapProperty.id;

          mProperty.goals = {};

          this.mArrayYears.forEach(mYear => {
            mProperty.goals[mYear] = {};

            for (let i = 0; i < this.mArrayMonths.length; i++) {
              mProperty.goals[mYear][i] = {};

              this.mArrayIncomeCategories.forEach(mCategory => {
                mProperty.goals[mYear][i][mCategory.id] = 0;
              });
            }

          });

          this.afService.db
            .collection('properties')
            .doc(mProperty.id)
            .collection('goals')
            .get()
            .then(mGoalsSnap => {


              mGoalsSnap.forEach(mYearGoal => {
                mProperty.goals[+mYearGoal.id] = mYearGoal.data();
              });

              this.mArrayProperties.push(mProperty);
            });



        });
      });
  }

  fnSaveGoal($event, mProperty, mYear, mMonth, mCategory) {
    const mAmount = +$event;
    if (!mAmount) {
      return 0;
    }
    if (mYear <= 2019 || mAmount <= 0) {
      return $event;
    }
    const mObj = mProperty.goals[mYear];
    mObj[mMonth][mCategory.id] = 0;
    mObj[mMonth][mCategory.id] = mAmount;
    console.log(mObj);
    this.afService.db
      .collection('properties')
      .doc(mProperty.id)
      .collection('goals')
      .doc(mYear.toString())
      .get()
      .then(mSnapGoalYear => {
        if (mSnapGoalYear.exists) {
          this.afService.db
            .collection('properties')
            .doc(mProperty.id)
            .collection('goals')
            .doc(mYear.toString())
            .update(
              mObj
            ).then(() => {
            return $event;
          });
        } else {
          this.afService.db
            .collection('properties')
            .doc(mProperty.id)
            .collection('goals')
            .doc(mYear.toString())
            .set(
              mObj
            ).then(() => {
            return $event;
          });
        }
      });
    return $event;
  }

}
