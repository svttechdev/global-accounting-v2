import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";
import {ActivatedRoute} from "@angular/router";
import {addDays, endOfMonth, getDaysInMonth, startOfDay, startOfMonth} from "date-fns";

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {

  mObjCategories: any;
  mArrayDays: Array<any>;
  mArrayIncomeCategories: Array<any>;
  mArrayExpensesCategories: Array<any>;

  mObjectMajorCat: any;
  mObjectMinorCat: any;
  mObjectTotalCategory: any;
  mObjectProperties: any;

  mStartOfMonth: number;
  mEndOfMonth: number;

  mTotal = {
    expenses: 0,
    income: 0
  };

  mReady: boolean;
  mArrayProperties: Array<any>;

  mGeneralTotalLabel = ['income', 'expenses', 'profit', '口座残高'];

  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  constructor(private afService: AuthenticationService) {
    this.mReady = true;
  }

  ngOnInit() {

    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }

    this.fnInitAll(new Date());

    this.afService.db
      .collection('major_subject')
      .get()
      .then(snapshot => {
        this.mArrayIncomeCategories = new Array<any>();
        this.mArrayExpensesCategories = new Array<any>();

        const tmpEmpty = {};
        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;
          tmpEmpty [mCategory.id] = mCategory;
          this.afService.db
            .collection('major_subject')
            .doc(mCategory.id)
            .collection('minor_subject')
            .get()
            .then(minorCategoriesSnapshot => {
              if (!tmpEmpty [mCategory.id] ['minor_subject']) {
                tmpEmpty [mCategory.id] ['minor_subject'] = {};
              }
              tmpEmpty [mCategory.id].minor_categories = [];
              minorCategoriesSnapshot.forEach(minorSnap => {
                const mMinorCategory = minorSnap.data();
                mMinorCategory.id = minorSnap.id;
                if (!tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id]) {
                  tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id] = mMinorCategory;
                  tmpEmpty [mCategory.id].minor_categories.push(mMinorCategory);
                }

              });
              if (mCategory.type === 'income') {
                this.mArrayIncomeCategories.push(tmpEmpty [mCategory.id]);
              } else if (mCategory.type === 'expenses') {
                this.mArrayExpensesCategories.push(tmpEmpty [mCategory.id]);
              }

            });
        });
        this.mObjCategories = tmpEmpty;
        console.log(tmpEmpty);
        this.fnGetRecords();
      });

  }

  fnInitAll(mDate: Date) {
    this.mStartOfMonth = +startOfMonth(new Date(mDate));
    this.mEndOfMonth = +endOfMonth(new Date(mDate));

    this.fnGetAllMonthDays(new Date(mDate));
  }

  fnGetRecords() {

    this.afService.db
      .collection('properties')
      .onSnapshot(snapshotProperties => {
        this.mArrayProperties = new Array<any>();

        const mObjRecordsMinor = {};
        const mObjRecordsMajor = {};
        const mObjTotalCategory = {};

        const mObjProperties = {};

        mObjProperties['total'] = {
          income: 0,
          expenses: 0,
          profit: 0
        };

        this.mTotal.expenses = 0;
        this.mTotal.income = 0;


        snapshotProperties.forEach(snapProperty => {
          const mProperty = snapProperty.data();
          mProperty.id = snapProperty.id;

          mObjProperties[mProperty.id] = {
            income: 0,
            expenses: 0,
            profit: 0
          };

          this.mArrayProperties.push(mProperty);

          this.afService.db
            .collection('properties')
            .doc(mProperty.id)
            .collection('record')
            .where('date', '>=', this.mStartOfMonth)
            .where('date', '<=', this.mEndOfMonth)
            .get()
            .then(snapshot => {

              snapshot.forEach(snap => {

                const mRecord = snap.data();
                mRecord.id = snap.id;

                const mIndex = +startOfDay(mRecord.date);
                // minor index
                if (!mObjRecordsMinor[mIndex]) {
                  mObjRecordsMinor[mIndex] = {};
                }
                if (mRecord.expense &&
                  mRecord.expense.key_major_subject &&
                  !mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject]) {
                  mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject] = {};
                }
                if (mRecord.expense &&
                  mRecord.expense.key_major_subject &&
                  mRecord.expense.key_minor_subject &&
                  !mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject]) {
                  mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject] = {
                    amount: 0
                  };
                }
                if (mRecord.income &&
                  mRecord.income.key_major_subject &&
                  !mObjRecordsMinor[mIndex][mRecord.income.key_major_subject]) {
                  mObjRecordsMinor[mIndex][mRecord.income.key_major_subject] = {};
                }
                if (mRecord.income &&
                  mRecord.income.key_major_subject &&
                  mRecord.income.key_minor_subject &&
                  !mObjRecordsMinor[mIndex][mRecord.income.key_major_subject][mRecord.income.key_minor_subject]) {
                  mObjRecordsMinor[mIndex][mRecord.income.key_major_subject][mRecord.income.key_minor_subject] = {
                    amount: 0
                  };
                }

                // major index
                if (!mObjRecordsMajor[mIndex]) {
                  mObjRecordsMajor[mIndex] = {};
                }
                if (mRecord.expense &&
                  mRecord.expense.key_major_subject &&
                  !mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject]) {

                  mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject] = {
                    amount: 0
                  };
                }
                if (mRecord.income &&
                  mRecord.income.key_major_subject &&
                  !mObjRecordsMajor[mIndex][mRecord.income.key_major_subject]) {

                  mObjRecordsMajor[mIndex][mRecord.income.key_major_subject] = {
                    amount: 0
                  };
                }

                if (!mObjRecordsMajor[mIndex]['total']) {
                  mObjRecordsMajor[mIndex]['total'] = {
                    income: 0,
                    expense: 0
                  };
                }

                if (!mObjProperties[mProperty.id][mIndex]) {
                  mObjProperties[mProperty.id][mIndex] = {
                    income: 0,
                    expenses: 0,
                    profit: 0
                  };
                }
                if (!mObjProperties['total'][mIndex]) {
                  mObjProperties['total'][mIndex] = {
                    income: 0,
                    expenses: 0,
                    profit: 0
                  };
                }

                // sum
                if (mRecord.income && mRecord.income.jpy) {
                  mObjRecordsMinor[mIndex][mRecord.income.key_major_subject][mRecord.income.key_minor_subject].amount += mRecord.income.jpy;
                  mObjRecordsMajor[mIndex][mRecord.income.key_major_subject].amount += mRecord.income.jpy;
                  if (mRecord.income.key_major_subject) {
                    mObjRecordsMajor[mIndex]['total'].income += mRecord.income.jpy;
                  }
                  if ( !mObjTotalCategory[mRecord.income.key_major_subject] ) {
                    mObjTotalCategory[mRecord.income.key_major_subject] = 0;
                  }
                  mObjTotalCategory[mRecord.income.key_major_subject] += mRecord.income.jpy;
                  this.mTotal.income += mRecord.income.jpy;
                  mObjProperties[mProperty.id][mIndex].income += mRecord.income.jpy;
                  mObjProperties['total'][mIndex].income += mRecord.income.jpy;
                  mObjProperties['total'].income += mRecord.income.jpy;
                  mObjProperties[mProperty.id].income += mRecord.income.jpy;
                }
                if (mRecord.expense && mRecord.expense.jpy) {
                  mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject].amount
                    += mRecord.expense.jpy;
                  mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject].amount += mRecord.expense.jpy;
                  if (mRecord.expense.key_major_subject) {
                    mObjRecordsMajor[mIndex]['total'].expense += mRecord.expense.jpy;
                  }
                  if ( !mObjTotalCategory[mRecord.expense.key_major_subject] ) {
                    mObjTotalCategory[mRecord.expense.key_major_subject] = 0;
                  }
                  mObjTotalCategory[mRecord.expense.key_major_subject] += mRecord.expense.jpy;
                  this.mTotal.expenses += mRecord.expense.jpy;
                  mObjProperties[mProperty.id][mIndex].expenses += mRecord.expense.jpy;
                  mObjProperties['total'][mIndex].expenses += mRecord.expense.jpy;
                  mObjProperties['total'].expenses += mRecord.expense.jpy;
                  mObjProperties[mProperty.id].expenses += mRecord.expense.jpy;
                }

                mObjProperties[mProperty.id][mIndex].profit
                  += (mRecord.income && mRecord.income.jpy ? mRecord.income.jpy : 0)
                  - (mRecord.expense && mRecord.expense.jpy ? mRecord.expense.jpy : 0);

                mObjProperties['total'][mIndex].profit
                  = (mObjProperties['total'][mIndex] && mObjProperties['total'][mIndex].income ? mObjProperties['total'][mIndex].income : 0)
                  - (mObjProperties['total'][mIndex] && mObjProperties['total'][mIndex].expenses ? mObjProperties['total'][mIndex].expenses : 0);

                mObjProperties['total'].profit
                  += (mRecord.income && mRecord.income.jpy ? mRecord.income.jpy : 0)
                  - (mRecord.expense && mRecord.expense.jpy ? mRecord.expense.jpy : 0);

                mObjProperties[mProperty.id].profit
                  += (mRecord.income && mRecord.income.jpy ? mRecord.income.jpy : 0)
                  - (mRecord.expense && mRecord.expense.jpy ? mRecord.expense.jpy : 0);

              });
              this.mObjectMajorCat = mObjRecordsMajor;
              this.mObjectMinorCat = mObjRecordsMinor;
              this.mObjectTotalCategory = mObjTotalCategory;
              this.mObjectProperties = mObjProperties;
            });

        });
      });
  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      this.mStartOfMonth = +startOfMonth(mNewDate);
      this.mEndOfMonth = +endOfMonth(mNewDate);

      this.fnInitAll(mNewDate);

      this.fnGetRecords();


    }
  }

  fnGetAllMonthDays(refDate: Date) {
    console.log(refDate);
    const tmpObj = {};
    const totalDays = +getDaysInMonth(refDate);
    let keyIndex = +startOfMonth(refDate);

    this.mArrayDays = new Array<any>();

    for (let i = 0; i < totalDays; i++) {
      tmpObj[keyIndex] = {};
      this.mArrayDays.push(keyIndex);
      keyIndex = +addDays(keyIndex, 1);
    }

    console.log(this.mArrayDays);
  }

}
