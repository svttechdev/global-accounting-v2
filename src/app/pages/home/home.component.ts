import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mArrayProperties: Array<any>;

  constructor(private afService: AuthenticationService) {
    this.afService.db
      .collection('properties')
      .orderBy('name', 'asc')
      .onSnapshot(snapshot => {

        this.mArrayProperties = new Array<any>();

        snapshot.forEach(snap => {
          const mProperty = snap.data();
          mProperty.id = snap.id;

          this.mArrayProperties.push(mProperty);
        });

      });
  }

  ngOnInit() {
  }

}
