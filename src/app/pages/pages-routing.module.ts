import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {FormComponent} from './form/form.component';
import {EAddCategoryComponent} from './tools/e-add-category/e-add-category.component';
import {EAddSubcategoryComponent} from './tools/e-add-subcategory/e-add-subcategory.component';
import {EAddBankAccountComponent} from './tools/e-add-bank-account/e-add-bank-account.component';
import {EAddContactComponent} from './tools/e-add-contact/e-add-contact.component';
import {CreatePropertyRecordComponent} from './property/create-property-record/create-property-record.component';
import {HomePropertyComponent} from './property/home-property/home-property.component';
import {PropertySummaryComponent} from './property/property-summary/property-summary.component';
import {TotalComponent} from './global/total/total.component';
import {PropertyDashboardComponent} from './property/property-dashboard/property-dashboard.component';
import {GlobalDashboardComponent} from './global/global-dashboard/global-dashboard.component';
import {GoalsComponent} from './global/goals/goals.component';
import {GoalsDashboardComponent} from './property/goals-dashboard/goals-dashboard.component';
import {PropertyReportComponent} from './property/property-report/property-report.component';

const pagesRoutes: Routes = [
  {path : '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'global/total', component: TotalComponent},
  {path: 'global/goals', component: GoalsComponent},
  {path: 'global/goals/dashboard/:id', component: GoalsDashboardComponent},
  {path: 'global/dashboard', component: GlobalDashboardComponent},
  {path: 'property-record/:id/daily', component: HomePropertyComponent},
  {path: 'property-record/:id/summary', component: PropertySummaryComponent},
  {path: 'property-record/:id/report', component: PropertyReportComponent},
  {path: 'property-record/:id/dashboard', component: PropertyDashboardComponent},
  {path: ':action/property-record', component: CreatePropertyRecordComponent},
  {path: 'form/:id/add-record/:action', component: FormComponent},
  // components
  {path: 'properties/add-category', component: EAddCategoryComponent},
  {path: 'properties/add-subcategory/:type', component: EAddSubcategoryComponent},
  {path: 'properties/add-bank-account/:key_property', component: EAddBankAccountComponent},
  {path: 'properties/add-contact', component: EAddContactComponent}
];
export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
