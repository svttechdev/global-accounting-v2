// Forms
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routes
import {PAGES_ROUTES} from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { EAddCategoryComponent } from './tools/e-add-category/e-add-category.component';
import { EAddSubcategoryComponent } from './tools/e-add-subcategory/e-add-subcategory.component';
import { EAddBankAccountComponent } from './tools/e-add-bank-account/e-add-bank-account.component';
import { EAddContactComponent } from './tools/e-add-contact/e-add-contact.component';
import { CreatePropertyRecordComponent } from './property/create-property-record/create-property-record.component';
import { HomePropertyComponent } from './property/home-property/home-property.component';
import { PropertySummaryComponent } from './property/property-summary/property-summary.component';
import { TotalComponent } from './global/total/total.component';
import { PropertyDashboardComponent } from './property/property-dashboard/property-dashboard.component';
import { GlobalDashboardComponent } from './global/global-dashboard/global-dashboard.component';
import { GoalsComponent } from './global/goals/goals.component';
import { GoalsDashboardComponent } from './property/goals-dashboard/goals-dashboard.component';
import { PropertyReportComponent } from './property/property-report/property-report.component';


// Components

@NgModule({
  declarations: [
  HomeComponent,
  FormComponent,
  EAddCategoryComponent,
  EAddSubcategoryComponent,
  EAddBankAccountComponent,
  EAddContactComponent,
  CreatePropertyRecordComponent,
  HomePropertyComponent,
  PropertySummaryComponent,
  TotalComponent,
  PropertyDashboardComponent,
  GlobalDashboardComponent,
  GoalsComponent,
  GoalsDashboardComponent,
  PropertyReportComponent,
  ],
  exports: [],
  imports: [
    CommonModule,
    PAGES_ROUTES,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class PagesModule { }
