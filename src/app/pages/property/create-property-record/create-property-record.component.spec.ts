import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePropertyRecordComponent } from './create-property-record.component';

describe('CreatePropertyRecordComponent', () => {
  let component: CreatePropertyRecordComponent;
  let fixture: ComponentFixture<CreatePropertyRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePropertyRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePropertyRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
