import { Component, OnInit } from '@angular/core';
import {PropertyRecord} from '../../../classes/property-record';
import {AuthenticationService} from "../../../services/authentication.service";

@Component({
  selector: 'app-create-property-record',
  templateUrl: './create-property-record.component.html',
  styleUrls: ['./create-property-record.component.css']
})
export class CreatePropertyRecordComponent implements OnInit {

  mPropertyRecord: PropertyRecord;

  constructor(private afService: AuthenticationService) { }

  ngOnInit() {
    this.mPropertyRecord = new PropertyRecord();
  }

  fnAddProperty() {
    console.log(this.mPropertyRecord);
    this.afService.db
      .collection('properties')
      .add(
        Object.assign({}, this.mPropertyRecord)
      )
      .then((d) => {
        console.log('added ' + d.id);
        this.mPropertyRecord = new PropertyRecord();
        this.afService.fnRedirect('home');
      });
  }
}
