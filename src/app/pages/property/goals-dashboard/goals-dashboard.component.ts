import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";
import * as crossfilter from "crossfilter";
import * as dc from "dc";
import * as d3Scale from "d3-scale";
import * as d3 from "d3";
import {addDays, endOfMonth, endOfYear, startOfDay, startOfMonth, startOfYear, subDays, subMonths} from "date-fns";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-goals-dashboard',
  templateUrl: './goals-dashboard.component.html',
  styleUrls: ['./goals-dashboard.component.css']
})
export class GoalsDashboardComponent implements OnInit {
  mArrayMonths = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mObjCategories: any;
  mArrayIncomeCategories: Array<any>;
  mArrayExpensesCategories: Array<any>;
  mArrayProperties: Array<any>;
  innerWidth: number;
  innerHeight: number;
  propertyId: string;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.propertyId = params['id'];
    });
    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }
    this.fnGetCategories();
    this.innerHeight = window.innerHeight;
    this.innerWidth = window.innerWidth;
  }

  fnGetCategories() {
    this.afService.db
      .collection('major_subject')
      .get()
      .then(snapshot => {
        this.mArrayIncomeCategories = new Array<any>();
        this.mArrayExpensesCategories = new Array<any>();

        const tmpEmpty = {};
        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;
          tmpEmpty [mCategory.id] = mCategory;
          if (mCategory.type === 'income') {
            this.mArrayIncomeCategories.push(tmpEmpty [mCategory.id]);
          } else if (mCategory.type === 'expenses') {
            this.mArrayExpensesCategories.push(tmpEmpty [mCategory.id]);
          }
        });

        this.mObjCategories = tmpEmpty;
        this.fnGetProperties();
      });
  }

  fnGetProperties() {
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .get()
      .then(snapProperty => {
        this.mArrayProperties = new Array<any>();

        const mProperty = snapProperty.data();
        mProperty.id = snapProperty.id;

        mProperty.goals = {};

        this.mArrayYears.forEach(mYear => {
          mProperty.goals[mYear] = {};

          for (let i = 0; i < this.mArrayMonths.length; i++) {
            mProperty.goals[mYear][i] = {};

            this.mArrayIncomeCategories.forEach(mCategory => {
              mProperty.goals[mYear][i][mCategory.id] = 0;
            });
          }

        });

        this.afService.db
          .collection('properties')
          .doc(mProperty.id)
          .collection('goals')
          .get()
          .then(mGoalsSnap => {
            mGoalsSnap.forEach(mYearGoal => {
              mProperty.goals[+mYearGoal.id] = mYearGoal.data();
            });

            this.mArrayProperties.push(mProperty);
          });
      });
  }

  fnSelectYear(mYear) {
    this.mSelectedDate.year = mYear;
  }

  fnSelectMonth(mMonth) {
    const mYear = this.mSelectedDate.year;
    this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

    if (this.mSelectedDate.year < 0 || this.mSelectedDate.month < 0) {
      return;
    }

    const mTmpDate = new Date().setFullYear(mYear);
    const mEndMonth = +endOfMonth(new Date(mTmpDate).setMonth(+this.mSelectedDate.month));

    const tmpArray = [];
    this.mArrayProperties.forEach(mProperty => {

      let mStartMonth = +startOfMonth(new Date(mTmpDate).setMonth(+this.mSelectedDate.month));

      while (mStartMonth <= mEndMonth) {
        tmpArray.push({
          id: mProperty.id,
          date: mStartMonth,
          value: mProperty.goals[mYear][this.mSelectedDate.month],
          is_goal: true
        });
        mStartMonth = +addDays(mStartMonth, 1);
      }
      // tmpObj[mProperty.id]['values'] = result;
    });
    // .where('date', '>=', +subMonths(this.mStartOfMonth, 1))
    const mDate = new Date();
    mDate.setFullYear(+mYear);
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('record')
      .where('date', '>=', this.afService.fnCreateDate(new Date(startOfMonth(mEndMonth))))
      .where('date', '<=', this.afService.fnCreateDate(new Date(mEndMonth)))
      .get()
      .then(snapRecords => {
        snapRecords.forEach(mSnapRecord => {

          const mRecord = mSnapRecord.data();
          if (mRecord.income && mRecord.income.key_major_subject) {
            const mTmp = {
              id: this.propertyId,
              date: +startOfDay(new Date(mRecord.date.toDate())),
              value: {},
              is_goal: false
            };
            mTmp['value'][mRecord.income.key_major_subject] = mRecord.income.jpy;

            tmpArray.push(mTmp);
          }

        });
        const ndx = crossfilter(tmpArray);
        const dateDimension = ndx.dimension((d) => {
          return d.date = new Date(d.date).getDate();
        });
        const maxDate = dateDimension.top(1)[0]['date'];
        const minDate = dateDimension.bottom(1)[0]['date'];

        this.mArrayIncomeCategories.forEach(mCategory => {
          this.render_graph_composite('composite-chart-' + mCategory.id, ndx, dateDimension, maxDate, minDate, mCategory.id);
        });

        dc.renderAll();
      });
  }

  render_graph_composite(idDiv: string, ndx, dateDimension, maxDate, minDate, keyCategory) {

    const domainX =
      d3Scale.scaleLinear()
        .domain([minDate, maxDate]);

    const compositeChart = dc.compositeChart('#' + idDiv);


    compositeChart
      .height(this.innerWidth / 4)
      .width(this.innerWidth * 0.8)
      .margins({top: 100, right: 10, bottom: 30, left: 80})
      .dimension(dateDimension)
      .x(domainX)
      .legend(dc.legend().x(90).y(20).itemHeight(15).gap(5))
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose([
        dc.barChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[1])
          .group(this.fnCreateCumulativeGroup(
            dateDimension.group().reduce(
              (p, v) => {
                if (v.value && v.value[keyCategory] && v.is_goal) {
                  p.total_reservation += +v.value[keyCategory] || 0;
                }
                p.total = (p.total_reservation);
                return p;
              },
              (p, v) => {
                if (v.value && v.value[keyCategory] && v.is_goal) {
                  p.total_reservation -= +v.value[keyCategory] || 0;
                }
                p.total = (p.total_reservation);
                return p;
              },
              () => {
                return {total_reservation: 0, total: 0};
              })
            ),
            'Goal')
          .x(domainX)
          .xUnits(dc.units.ordinal),
        dc.barChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[0])
          .group(this.fnCreateTrueCumulativeGroup(
            dateDimension.group().reduce(
              (p, v) => {
                if (v.value && v.value[keyCategory] && !v.is_goal) {
                  p.total_reservation += +v.value[keyCategory] || 0;
                }
                p.total = (p.total_reservation);
                return p;
              },
              (p, v) => {
                if (v.value && v.value[keyCategory] && !v.is_goal) {
                  p.total_reservation -= +v.value[keyCategory] || 0;
                }
                p.total = (p.total_reservation);
                return p;
              },
              () => {
                return {total_reservation: 0, total: 0};
              })
            ),
            'Income')
          .x(domainX)
          .xUnits(dc.units.ordinal)
      ])
      .brushOn(false)
      .mouseZoomable(false)
      .zoomOutRestrict(true);

  }

  fnCreateCumulativeGroup(source_group) {
    return {
      all: () => {
        const cumulate = {};
        return source_group.all().map((d) => {
          if (cumulate[d.key[0]]) {
            cumulate[d.key[0]] = d.value.total;
          } else {
            cumulate[d.key[0]] = d.value.total;
          }
          return {key: d.key, value: cumulate[d.key[0]]};
        });
      }
    };
  }

  fnCreateTrueCumulativeGroup(source_group) {
    return {
      all: () => {
        const cumulate = {};
        return source_group.all().map((d) => {
          if (cumulate[d.key[0]]) {
            cumulate[d.key[0]] += d.value.total;
          } else {
            cumulate[d.key[0]] = d.value.total;
          }
          return {key: d.key, value: cumulate[d.key[0]]};
        });
      }
    };
  }

}
