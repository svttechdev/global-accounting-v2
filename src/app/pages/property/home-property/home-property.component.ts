import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {addMonths, endOfDay, endOfMonth, startOfDay, startOfMonth, subMonths, toDate} from 'date-fns';
import {ExportToCsv} from "export-to-csv";

@Component({
  selector: 'app-home-property',
  templateUrl: './home-property.component.html',
  styleUrls: ['./home-property.component.css']
})
export class HomePropertyComponent implements OnInit, AfterViewInit {

  propertyId: string;

  mArrayRecord: Array<any>;
  mObjCategories: any;
  mObjBankAccounts: any;
  mObjContacts: any;

  mArrayBankAccount: Array<any>;
  mArrayBankAccountDisplay: Array<any>;

  mPreviousMonth: number;
  mCurrentMonth: number;

  mStartOfMonth: number;
  mEndOfMonth: number;
  mCurrentDate: number;
  mCurrentDateStatic: number;
  mTotalBalance: any;

  mRecordsReference: any;
  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };
  mSelectedMonth: string;

  mProperty: any;
  mArrayIncomeCategories: Array<any>;
  mTotalIncome: any;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {

    this.mArrayBankAccountDisplay = new Array<any>();
    this.mCurrentDateStatic = +startOfDay(new Date())
    this.mStartOfMonth = +startOfMonth(new Date());
    this.mEndOfMonth = +endOfMonth(new Date());
    this.mCurrentDate = +endOfDay(new Date());

    this.mSelectedDate.year = new Date(this.mStartOfMonth).getFullYear();
    this.mSelectedDate.month = new Date(this.mStartOfMonth).getMonth();

    this.mPreviousMonth = (new Date().getMonth() - 1) % 11;
    this.mCurrentMonth = new Date().getMonth();

    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }

    this.route.params.subscribe(params => {
      this.propertyId = params['id'];
      this.afService.mSelectedPropertyID = this.propertyId;
      this.afService.db
        .collection('properties')
        .doc(this.propertyId)
        .get()
        .then(snapProperty => {
          this.mProperty = snapProperty.data();
          this.mProperty.goals = {};
          this.mRecordsReference = this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('record');
          this.fnInitAll();
        });
    });
  }

  fnInitAll() {
    this.mRecordsReference
      .orderBy('date', 'asc')
      .where('date', '>=', this.afService.fnCreateDate(subMonths(this.mStartOfMonth, 1)))
      .where('date', '<', this.afService.fnCreateDate(startOfMonth(addMonths(this.mStartOfMonth, 1))))
      .onSnapshot(snapshot => {

        this.mArrayRecord = new Array<any>();
        const mTotalIncome = {};

        let mIndex = 0;

        snapshot.forEach(snap => {
          const mRecord = snap.data();
          mRecord.id = snap.id;

          console.log(mRecord);
          mRecord.date = mRecord.date.toDate();
          if (mRecord.income && mRecord.income.paymentDate) {
            mRecord.income.paymentDate = mRecord.income.paymentDate.toDate();
          }
          if (mRecord.expense && mRecord.expense.withdrawalDate) {
            mRecord.expense.withdrawalDate = mRecord.expense.withdrawalDate.toDate();
          }

          mRecord.currentMonth = new Date(mRecord.date).getMonth();
          mRecord.totalBalance = {
            localCurrency: 0,
            jpy: 0
          };

          if (mRecord.currentMonth === this.mCurrentMonth) {

            if (mRecord.expense && mRecord.expense.jpy) {
              mRecord.totalBalance.jpy -= (+mRecord.expense.jpy);
            }
            if (mRecord.expense && mRecord.expense.localCurrency) {
              mRecord.totalBalance.localCurrency -= (+mRecord.expense.localCurrency);
            }
            if (mRecord.income && mRecord.income.jpy) {
              mRecord.totalBalance.jpy += (+mRecord.income.jpy);

              if (mRecord.income.key_major_subject) {
                if (!mTotalIncome[mRecord.income.key_major_subject]) {
                  mTotalIncome[mRecord.income.key_major_subject] = 0;
                }
                mTotalIncome[mRecord.income.key_major_subject] += (+mRecord.income.jpy);
              }

            }
            if (mRecord.income && mRecord.income.localCurrency) {
              mRecord.totalBalance.localCurrency += (+mRecord.income.localCurrency);
            }

            if (mIndex !== 0) {
              mRecord.totalBalance.jpy += this.mArrayRecord[this.mArrayRecord.length - 1].totalBalance.jpy;
              mRecord.totalBalance.localCurrency += this.mArrayRecord[this.mArrayRecord.length - 1].totalBalance.localCurrency;
            }
            mIndex++;
          }

          this.mArrayRecord.push(mRecord);

        });
        this.mTotalIncome = mTotalIncome;
        console.log(this.mTotalIncome);
        console.log(this.mArrayRecord);

        this.afService.db
          .collection('properties')
          .doc(this.propertyId)
          .collection('bank_account')
          .get()
          .then(bSnapshot => {
            const tmpEmpty = {};
            this.mArrayBankAccount = new Array<any>();
            bSnapshot.forEach(bSnap => {
              const mBankAccount = bSnap.data();
              mBankAccount.id = bSnap.id;
              this.mArrayBankAccount.push(mBankAccount);
              tmpEmpty [mBankAccount.id] = mBankAccount;
            });
            this.mObjBankAccounts = tmpEmpty;
            this.fnPreSelectBankAccount();
            this.fnJoinByBankAccount();
          });
        this.afService.db
          .collection('properties')
          .doc(this.propertyId)
          .collection('goals')
          .get()
          .then(mGoalsSnap => {
            mGoalsSnap.forEach(mYearGoal => {
              this.mProperty.goals[+mYearGoal.id] = mYearGoal.data();
            });
            console.log(this.mProperty);
          });

      });
    this.afService.db
      .collection('major_subject')
      .get()
      .then(snapshot => {

        const tmpEmpty = {};
        this.mArrayIncomeCategories = new Array<any>();

        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;
          tmpEmpty [mCategory.id] = mCategory;

          if (mCategory.type === 'income') {
            this.mArrayIncomeCategories.push(tmpEmpty [mCategory.id]);
          }

          this.afService.db
            .collection('major_subject')
            .doc(mCategory.id)
            .collection('minor_subject')
            .get()
            .then(minorCategoriesSnapshot => {
              if (!tmpEmpty [mCategory.id] ['minor_subject']) {
                tmpEmpty [mCategory.id] ['minor_subject'] = {};
              }
              minorCategoriesSnapshot.forEach(minorSnap => {
                const mMinorCategory = minorSnap.data();
                mMinorCategory.id = minorSnap.id;
                if (!tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id]) {
                  tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id] = mMinorCategory;
                }
              });
            });
        });
        this.mObjCategories = tmpEmpty;
      });

    this.afService.db
      .collection('contact')
      .get()
      .then(snapshot => {
        const tmpEmpty = {};
        snapshot.forEach(snap => {
          const mContact = snap.data();
          mContact.id = snap.id;
          tmpEmpty [mContact.id] = mContact;
        });
        this.mObjContacts = tmpEmpty;
      });
  }

  fnPreSelectBankAccount() {
    this.mArrayBankAccountDisplay = new Array<any>();
    console.log(this.mArrayBankAccount);
    if (this.mArrayBankAccount[0]) {
      this.mArrayBankAccountDisplay[0] = this.mArrayBankAccount[0];
    }
    if (this.mArrayBankAccount[1]) {
      this.mArrayBankAccountDisplay[1] = this.mArrayBankAccount[1];
    }
    if (this.mArrayBankAccount[2]) {
      this.mArrayBankAccountDisplay[2] = this.mArrayBankAccount[2];
    }
  }

  fnBankAccountSelected($event, index) {

    this.mArrayBankAccountDisplay[index] = this.mObjBankAccounts[$event];

    return this.mArrayBankAccountDisplay[index];
  }

  fnJoinByBankAccount() {

    const mStartLM = +subMonths(this.mStartOfMonth, 1);

    this.mArrayBankAccountDisplay.forEach(displayBank => {
      if (displayBank.lmBalance) {
        displayBank.lmBalance = null;
      }
      if (displayBank.inputBalance) {
        displayBank.inputBalance = null;
      }
      if (displayBank.toDate) {
        displayBank.toDate = null;
      }
    });

    const mTotalBalance = {
      lmBalance: {
        expenses: {
          localCurrency: 0,
          jpy: 0
        },
        income: {
          localCurrency: 0,
          jpy: 0
        }
      },
      inputBalance: {
        expenses: {
          localCurrency: 0,
          jpy: 0
        },
        income: {
          localCurrency: 0,
          jpy: 0
        }
      },
      toDate: {
        expenses: {
          localCurrency: 0,
          jpy: 0
        },
        income: {
          localCurrency: 0,
          jpy: 0
        }
      }
    };
    this.mArrayRecord.forEach(record => {
      const mRecordDate = +new Date(+record.date);

      this.mArrayBankAccount.forEach(displayBank => {

        if (!displayBank.lmBalance) {
          displayBank.lmBalance = {
            expenses: {
              localCurrency: 0,
              jpy: 0
            },
            income: {
              localCurrency: 0,
              jpy: 0
            }
          };
        }
        if (!displayBank.inputBalance) {
          displayBank.inputBalance = {
            expenses: {
              localCurrency: 0,
              jpy: 0
            },
            income: {
              localCurrency: 0,
              jpy: 0
            }
          };
        }
        if (!displayBank.toDate) {
          displayBank.toDate = {
            expenses: {
              localCurrency: 0,
              jpy: 0
            },
            income: {
              localCurrency: 0,
              jpy: 0
            }
          };
        }

        if (record.expense.key_bank_account === displayBank.id && mRecordDate >= mStartLM && mRecordDate < this.mStartOfMonth) {
          displayBank.lmBalance.expenses.localCurrency +=
            (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
          displayBank.lmBalance.expenses.jpy +=
            (record.expense && record.expense.jpy ? record.expense.jpy : 0);
        }
        if (record.income.key_bank_account === displayBank.id && mRecordDate >= mStartLM && mRecordDate < this.mStartOfMonth) {
          displayBank.lmBalance.income.localCurrency +=
            (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
          displayBank.lmBalance.income.jpy +=
            (record.income && record.income.jpy ? record.income.jpy : 0);
        }

        if (record.expense.key_bank_account === displayBank.id && mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mEndOfMonth) {
          displayBank.inputBalance.expenses.localCurrency +=
            (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
          displayBank.inputBalance.expenses.jpy +=
            (record.expense && record.expense.jpy ? record.expense.jpy : 0);
        }
        if (record.income.key_bank_account === displayBank.id && mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mEndOfMonth) {
          displayBank.inputBalance.income.localCurrency +=
            (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
          displayBank.inputBalance.income.jpy +=
            (record.income && record.income.jpy ? record.income.jpy : 0);
        }

        if (record.expense.key_bank_account === displayBank.id && mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mCurrentDate) {
          displayBank.toDate.expenses.localCurrency +=
            (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
          displayBank.toDate.expenses.jpy +=
            (record.expense && record.expense.jpy ? record.expense.jpy : 0);
        }
        if (record.income.key_bank_account === displayBank.id && mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mCurrentDate) {
          displayBank.toDate.income.localCurrency +=
            (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
          displayBank.toDate.income.jpy +=
            (record.income && record.income.jpy ? record.income.jpy : 0);
        }

      });
      // end for each accounts
      if (mRecordDate >= mStartLM && mRecordDate < this.mStartOfMonth) {
        mTotalBalance.lmBalance.expenses.localCurrency +=
          (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
        mTotalBalance.lmBalance.expenses.jpy +=
          (record.expense && record.expense.jpy ? record.expense.jpy : 0);
      }
      if (mRecordDate >= mStartLM && mRecordDate < this.mStartOfMonth) {
        mTotalBalance.lmBalance.income.localCurrency +=
          (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
        mTotalBalance.lmBalance.income.jpy +=
          (record.income && record.income.jpy ? record.income.jpy : 0);
      }

      if (mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mEndOfMonth) {
        mTotalBalance.inputBalance.expenses.localCurrency +=
          (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
        mTotalBalance.inputBalance.expenses.jpy +=
          (record.expense && record.expense.jpy ? record.expense.jpy : 0);
      }
      if (mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mEndOfMonth) {
        mTotalBalance.inputBalance.income.localCurrency +=
          (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
        mTotalBalance.inputBalance.income.jpy +=
          (record.income && record.income.jpy ? record.income.jpy : 0);
      }

      if (mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mCurrentDate) {
        mTotalBalance.toDate.expenses.localCurrency +=
          (record.expense && record.expense.localCurrency ? record.expense.localCurrency : 0);
        mTotalBalance.toDate.expenses.jpy +=
          (record.expense && record.expense.jpy ? record.expense.jpy : 0);
      }
      if (mRecordDate >= this.mStartOfMonth && mRecordDate <= this.mCurrentDate) {
        mTotalBalance.toDate.income.localCurrency +=
          (record.income && record.income.localCurrency ? record.income.localCurrency : 0);
        mTotalBalance.toDate.income.jpy +=
          (record.income && record.income.jpy ? record.income.jpy : 0);
      }

    });
    this.mTotalBalance = mTotalBalance;
  }

  fnDeleteRecord(record) {
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('record')
      .doc(record.id)
      .delete().then(() => {
      console.log('Document successfully deleted!');
    }).catch((error) => {
      console.error('Error removing document: ', error);
    });
  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      this.mStartOfMonth = +startOfMonth(mNewDate);
      this.mEndOfMonth = +endOfMonth(mNewDate);

      if (mMonth === new Date().getMonth()) {
        this.mCurrentDate = +endOfDay(new Date());
      } else {
        this.mCurrentDate = this.mEndOfMonth;
      }

      this.mCurrentMonth = new Date(this.mCurrentDate).getMonth();

      this.mRecordsReference
        .orderBy('date', 'asc')
        .where('date', '>=', this.afService.fnCreateDate(subMonths(this.mStartOfMonth, 1)))
        .where('date', '<', this.afService.fnCreateDate(startOfMonth(addMonths(this.mStartOfMonth, 1))))
        .onSnapshot(snapshot => {

          this.mArrayRecord = new Array<any>();
          const mTotalIncome = {};

          let mIndex = 0;

          snapshot.forEach(snap => {
            const mRecord = snap.data();
            mRecord.id = snap.id;

            mRecord.date = mRecord.date.toDate();
            if (mRecord.income && mRecord.income.paymentDate) {
              mRecord.income.paymentDate = mRecord.income.paymentDate.toDate();
            }
            if (mRecord.expense && mRecord.expense.withdrawalDate) {
              mRecord.expense.withdrawalDate = mRecord.expense.withdrawalDate.toDate();
            }

            mRecord.currentMonth = new Date(mRecord.date).getMonth();
            mRecord.totalBalance = {
              localCurrency: 0,
              jpy: 0
            };

            if (mRecord.currentMonth === this.mCurrentMonth) {

              if (mRecord.expense && mRecord.expense.jpy) {
                mRecord.totalBalance.jpy -= (+mRecord.expense.jpy);
              }
              if (mRecord.expense && mRecord.expense.localCurrency) {
                mRecord.totalBalance.localCurrency -= (+mRecord.expense.localCurrency);
              }
              if (mRecord.income && mRecord.income.jpy) {
                mRecord.totalBalance.jpy += (+mRecord.income.jpy);

                if (mRecord.income.key_major_subject) {
                  if (!mTotalIncome[mRecord.income.key_major_subject]) {
                    mTotalIncome[mRecord.income.key_major_subject] = 0;
                  }
                  mTotalIncome[mRecord.income.key_major_subject] += (+mRecord.income.jpy);
                }

              }
              if (mRecord.income && mRecord.income.localCurrency) {
                mRecord.totalBalance.localCurrency += (+mRecord.income.localCurrency);
              }

              if (mIndex !== 0) {
                mRecord.totalBalance.jpy += this.mArrayRecord[this.mArrayRecord.length - 1].totalBalance.jpy;
                mRecord.totalBalance.localCurrency += this.mArrayRecord[this.mArrayRecord.length - 1].totalBalance.localCurrency;
              }
              mIndex++;
            }

            this.mArrayRecord.push(mRecord);

          });
          this.mTotalIncome = mTotalIncome;
          console.log(this.mTotalIncome);
          console.log(this.mArrayRecord);

          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('bank_account')
            .get()
            .then(bSnapshot => {
              const tmpEmpty = {};
              this.mArrayBankAccount = new Array<any>();
              bSnapshot.forEach(bSnap => {
                const mBankAccount = bSnap.data();
                mBankAccount.id = bSnap.id;
                this.mArrayBankAccount.push(mBankAccount);
                tmpEmpty [mBankAccount.id] = mBankAccount;
              });
              this.mObjBankAccounts = tmpEmpty;
              this.fnPreSelectBankAccount();
              this.fnJoinByBankAccount();
            });
          this.afService.db
            .collection('properties')
            .doc(this.propertyId)
            .collection('goals')
            .get()
            .then(mGoalsSnap => {
              mGoalsSnap.forEach(mYearGoal => {
                this.mProperty.goals[+mYearGoal.id] = mYearGoal.data();
              });
              console.log(this.mProperty);
            });

        });
    }
  }

  fnAddRecord() {
    this.afService.fnRedirect('/form/' + this.propertyId + '/add-record/new');
  }

  fnExportToCsv() {
    if (this.mArrayRecord && this.mArrayRecord.length > 0) {
      const mTmpArray = new Array();
      this.mArrayRecord.forEach(mRecord => {
        mTmpArray.push(
          {
            Schedule: (mRecord.date ? new Date(mRecord.date) : ''),
            ExpMajorSubject: (this.mObjCategories && this.mObjCategories[mRecord.expense.key_major_subject] ? this.mObjCategories[mRecord.expense.key_major_subject].name : ''),
            ExpSmallSubject: (this.mObjCategories && this.mObjCategories[mRecord.expense.key_major_subject] && this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] && this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] [mRecord.expense.key_minor_subject] ? this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] [mRecord.expense.key_minor_subject].name : ''),
            ExpSuppliers: mRecord.expense.suppliers,
            ExpRemarks: mRecord.expense.remarks,
            ExpLocalCurrency: mRecord.expense.localCurrency,
            ExpJPY: mRecord.expense.jpy,
            ExpBankAccount: (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.expense.key_bank_account] ? this.mObjBankAccounts && this.mObjBankAccounts[mRecord.expense.key_bank_account].name : ''),
            ExpContactName: (this.mObjContacts && this.mObjContacts[mRecord.income.key_contact] ? this.mObjContacts[mRecord.income.key_contact].name : ''),
            ExpWithdrawalDate: (mRecord.expense.withdrawalDate ? mRecord.expense.withdrawalDate : ''),
            IncMajorSubject: (this.mObjCategories && this.mObjCategories[mRecord.income.key_major_subject] ? this.mObjCategories[mRecord.income.key_major_subject].name : ''),
            IncSmallSubject: (this.mObjCategories && this.mObjCategories[mRecord.income.key_major_subject] && this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] && this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] [mRecord.income.key_minor_subject] ? this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] [mRecord.income.key_minor_subject].name : ''),
            IncSuppliers: mRecord.income.suppliers,
            IncRemarks: mRecord.income.remarks,
            IncLocalCurrency: mRecord.income.localCurrency,
            IncJPY: mRecord.income.jpy,
            IncBankAccount: (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.income.key_bank_account] ? this.mObjBankAccounts && this.mObjBankAccounts[mRecord.income.key_bank_account].name : ''),
            IncContactName: (this.mObjContacts && this.mObjContacts[mRecord.income.key_contact] ? this.mObjContacts[mRecord.income.key_contact].name : ''),
            IncPaymentDate: (mRecord.income.paymentDate ? mRecord.income.paymentDate : ''),
            // TotalLocalCurrency: (mRecord.income.jpy ? mRecord.income.localCurrency : 0) - (mRecord.expense.localCurrency ? mRecord.expense.localCurrency : 0),
            // TotalJPY: (mRecord.income.jpy ? mRecord.income.jpy : 0) - (mRecord.expense.jpy ? mRecord.expense.jpy : 0),
            BalanceLocalCurrency: (mRecord.totalBalance.localCurrency ? mRecord.totalBalance.localCurrency : 0),
            BalanceJPY: (mRecord.totalBalance.jpy ? mRecord.totalBalance.jpy : 0)
          }
        );
      });

      const options = {
        filename: (+startOfDay(new Date())).toString(),
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true,
        showTitle: true,
        title: new Date().toString(),
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
      };

      const csvExporter = new ExportToCsv(options);

      csvExporter.generateCsv(mTmpArray);
    }
  }

  fnExportToCsvSmasllCash() {
    if (this.mArrayRecord && this.mArrayRecord.length > 0) {
      const mTmpArray = new Array();
      this.mArrayRecord.forEach(mRecord => {

        if (
          (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.expense.key_bank_account] &&
            this.mObjBankAccounts[mRecord.expense.key_bank_account].name === 'Small Cash')
          ||
          (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.income.key_bank_account] &&
            this.mObjBankAccounts[mRecord.income.key_bank_account].name === 'Small Cash')
        ) {
          console.log(mRecord)
          mTmpArray.push(
            {
              Schedule: (mRecord.date ? new Date(mRecord.date) : ''),
              ExpMajorSubject: (this.mObjCategories && this.mObjCategories[mRecord.expense.key_major_subject] ? this.mObjCategories[mRecord.expense.key_major_subject].name : ''),
              ExpSmallSubject: (this.mObjCategories && this.mObjCategories[mRecord.expense.key_major_subject] && this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] && this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] [mRecord.expense.key_minor_subject] ? this.mObjCategories[mRecord.expense.key_major_subject] ['minor_subject'] [mRecord.expense.key_minor_subject].name : ''),
              ExpSuppliers: mRecord.expense.suppliers,
              ExpRemarks: mRecord.expense.remarks,
              ExpLocalCurrency: mRecord.expense.localCurrency,
              ExpJPY: mRecord.expense.jpy,
              ExpBankAccount: (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.expense.key_bank_account] ? this.mObjBankAccounts && this.mObjBankAccounts[mRecord.expense.key_bank_account].name : ''),
              ExpContactName: (this.mObjContacts && this.mObjContacts[mRecord.income.key_contact] ? this.mObjContacts[mRecord.income.key_contact].name : ''),
              ExpWithdrawalDate: (mRecord.expense.withdrawalDate ? mRecord.expense.withdrawalDate : ''),
              IncMajorSubject: (this.mObjCategories && this.mObjCategories[mRecord.income.key_major_subject] ? this.mObjCategories[mRecord.income.key_major_subject].name : ''),
              IncSmallSubject: (this.mObjCategories && this.mObjCategories[mRecord.income.key_major_subject] && this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] && this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] [mRecord.income.key_minor_subject] ? this.mObjCategories[mRecord.income.key_major_subject] ['minor_subject'] [mRecord.income.key_minor_subject].name : ''),
              IncSuppliers: mRecord.income.suppliers,
              IncRemarks: mRecord.income.remarks,
              IncLocalCurrency: mRecord.income.localCurrency,
              IncJPY: mRecord.income.jpy,
              IncBankAccount: (this.mObjBankAccounts && this.mObjBankAccounts[mRecord.income.key_bank_account] ? this.mObjBankAccounts && this.mObjBankAccounts[mRecord.income.key_bank_account].name : ''),
              IncContactName: (this.mObjContacts && this.mObjContacts[mRecord.income.key_contact] ? this.mObjContacts[mRecord.income.key_contact].name : ''),
              IncPaymentDate: (mRecord.income.paymentDate ? mRecord.income.paymentDate : ''),
              BalanceLocalCurrency: (mRecord.totalBalance.localCurrency ? mRecord.totalBalance.localCurrency : 0),
              BalanceJPY: (mRecord.totalBalance.jpy ? mRecord.totalBalance.jpy : 0)
            }
          );
        }
      });

      const options = {
        filename: (+startOfDay(new Date())).toString(),
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true,
        showTitle: true,
        title: new Date().toString(),
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
      };

      const csvExporter = new ExportToCsv(options);

      csvExporter.generateCsv(mTmpArray);
    }
  }
}
