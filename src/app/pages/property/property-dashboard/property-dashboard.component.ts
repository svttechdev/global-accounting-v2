import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {addDays, endOfDay, endOfMonth, startOfDay, startOfMonth} from 'date-fns';
import * as dc from 'dc';
import * as crossfilter from 'crossfilter';
import * as d3 from 'd3';
import * as d3Time_ from 'd3-time';
import * as d3Scale from 'd3-scale';

@Component({
  selector: 'app-property-dashboard',
  templateUrl: './property-dashboard.component.html',
  styleUrls: ['./property-dashboard.component.css']
})
export class PropertyDashboardComponent implements OnInit {

  propertyId: string;
  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mArrayRecords: Array<any>;
  mArrayMajorSubjects: Array<any>;
  mObjectMajorSubjects: any;

  mArrayBanks: Array<any>;
  mObjectBanks: any;

  innerWidth: number;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }


    this.route.params.subscribe(params => {

      this.propertyId = params.id;
      this.afService.mSelectedPropertyID = this.propertyId;
      this.fnPromiseGetCategories()
        .then(result => {
          console.log(result);
        });
      this.fnPromiseGetBanks()
        .then(result => {
          console.log(result);
        });
    });
  }

  fnPromiseGetCategories() {
    return new Promise(resolve => {
      this.afService.db
        .collection('major_subject')
        .get()
        .then(snapshot => {
          this.mArrayMajorSubjects = new Array<any>();
          this.mObjectMajorSubjects = {};
          const mTmpObj = {};
          snapshot.forEach(majorSubject => {
            const mMajorsubject = majorSubject.data();
            mMajorsubject.id = majorSubject.id;
            mTmpObj[mMajorsubject.id] = mMajorsubject;
            console.log(mMajorsubject);
            this.mArrayMajorSubjects.push(mMajorsubject);
          });
          this.mObjectMajorSubjects = mTmpObj;
          resolve(true);
        });
    });
  }

  fnPromiseGetBanks() {
    return new Promise(resolve => {
      this.afService.db
        .collection('properties')
        .doc(this.propertyId)
        .collection('bank_account')
        .get()
        .then(snapshotBankAccounts => {

          this.mArrayBanks = new Array<any>();
          this.mObjectBanks = {};
          const mTmpObj = {};
          snapshotBankAccounts.forEach(bankSnap => {
            const mBank = bankSnap.data();
            mBank.id = bankSnap.id;
            mTmpObj[mBank.id] = mBank;
            this.mArrayBanks.push(mBank);
          });
          this.mObjectBanks = mTmpObj;

          resolve(true);
        });
    });
  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      this.fnGetRecords(mNewDate);
    }
  }

  fnGetRecords(date: Date) {
    const mStartMonth = +startOfMonth(date);
    const mEndMonth = +endOfMonth(date);

    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('record')
      .where('date', '>=', this.afService.fnCreateDate(new Date(mStartMonth)))
      .where('date', '<=', this.afService.fnCreateDate(new Date(mEndMonth)))
      .get()
      .then(snapshot => {

        this.mArrayRecords = new Array<any>();
        let mIterator = +startOfMonth(date);

        while (mIterator <= mEndMonth) {

          this.mArrayRecords.push(
            {
              expense: {
                key: '',
                date: +endOfDay(mIterator)
              },
              date: +endOfDay(mIterator)
            }
          );

          mIterator = +addDays(mIterator, 1);
        }

        snapshot.forEach(record => {
          const mRecord = record.data();
          mRecord.id = record.id;
          mRecord.date = +endOfDay(mRecord.date.toDate());

          console.log(mRecord);

          if (mRecord.expense &&
            mRecord.expense.jpy && mRecord.expense.localCurrency &&
            mRecord.expense.key_major_subject && mRecord.expense.key_minor_subject &&
            mRecord.expense.key_bank_account) {
            mRecord.expense.date = mRecord.date;
            mRecord.expense.key = 'expense';
            mRecord.expense.id_major_subject = mRecord.income.key_major_subject;
            mRecord.expense.key_major_subject = this.mObjectMajorSubjects[mRecord.expense.key_major_subject].name;
            mRecord.expense.key_bank_account = this.mObjectBanks[mRecord.expense.key_bank_account].name;
            this.mArrayRecords.push(
              mRecord.expense
            );
          }

          if (mRecord.income &&
            mRecord.income.jpy && mRecord.income.localCurrency &&
            mRecord.income.key_major_subject && mRecord.income.key_minor_subject &&
            mRecord.income.key_bank_account) {
            mRecord.income.date = mRecord.date;
            mRecord.income.key = 'income';
            mRecord.income.id_major_subject = mRecord.income.key_major_subject;
            mRecord.income.key_major_subject = this.mObjectMajorSubjects[mRecord.income.key_major_subject].name;
            mRecord.income.key_bank_account = this.mObjectBanks[mRecord.income.key_bank_account].name;
            this.mArrayRecords.push(
              mRecord.income
            );
          }

        });
        this.fnInitGraphs();
      });
  }

  fnInitGraphs() {
    const ndx = crossfilter(this.mArrayRecords);

    const dateDimension = ndx.dimension((d) => {
      return d.date;
    });
    const maxDate = dateDimension.top(1)[0]['date'];
    const minDate = dateDimension.bottom(1)[0]['date'];


    this.render_graph_composite('incomes-expenses-chart', dateDimension, minDate, maxDate);
    this.render_major_categories('major-categories-chart', ndx);
    this.render_bank_account('bank-accounts-chart', ndx);

    this.render_graph_composite_incomes('incomes-chart', dateDimension, minDate, maxDate);
    this.render_graph_composite_expenses('expenses-chart', dateDimension, minDate, maxDate);

    dc.renderAll();
  }

  render_graph_composite_incomes(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 100, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayMajorSubjects.forEach(mSubj => {
      if (mSubj.type === 'income') {
        const incomes = dateDimension.group().reduce(
          (p, v) => {
            p.total += (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          (p, v) => {
            p.total -= (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          () => {
            return {total: 0};
          }
        );
        mTmpArray.push(
          dc.lineChart(compositeChart)
            .dimension(dateDimension)
            .colors(d3.schemeSet1[mTmpArray.length])
            .group(incomes, mSubj.name)
            .valueAccessor(d => {
              return d.value.total;
            })
        );
      }
    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite_expenses(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 100, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    this.mArrayMajorSubjects.forEach(mSubj => {
      if (mSubj.type === 'expenses') {
        const incomes = dateDimension.group().reduce(
          (p, v) => {
            p.total += (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          (p, v) => {
            p.total -= (v['key_major_subject'] === mSubj.name ? v['jpy'] : 0);
            return p;
          },
          () => {
            return {total: 0};
          }
        );
        mTmpArray.push(
          dc.lineChart(compositeChart)
            .dimension(dateDimension)
            .colors(d3.schemeSet1[mTmpArray.length])
            .group(incomes, mSubj.name)
            .valueAccessor(d => {
              return d.value.total;
            })
        );
      }
    });

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }

  render_graph_composite(idDiv: string, dateDimension, minDate, maxDate) {

    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const incomes = dateDimension.group().reduce(
      (p, v) => {
        p.total += (v['key'] === 'income' ? v['jpy'] : 0);
        return p;
      },
      (p, v) => {
        p.total -= (v['key'] === 'income' ? v['jpy'] : 0);
        return p;
      },
      () => {
        return {total: 0};
      }
    );

    const expenses = dateDimension.group().reduce(
      (p, v) => {
        p.total += (v['key'] === 'expense' ? v['jpy'] : 0);
        return p;
      },
      (p, v) => {
        p.total -= (v['key'] === 'expense' ? v['jpy'] : 0);
        return p;
      },
      () => {
        return {total: 0};
      }
    );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 100, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(65).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose([
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[0])
          .group(incomes, 'Incomes')
          .valueAccessor(d => {
            return d.value.total;
          }),
        dc.lineChart(compositeChart)
          .dimension(dateDimension)
          .colors(d3.schemeSet1[1])
          .group(expenses, 'Expenses')
          .valueAccessor(d => {
            return d.value.total;
          })
      ])
      .brushOn(false);

  }

  render_major_categories(idDiv: string, ndx) {
    const runDimension = ndx.dimension(d => {
      return d['key_major_subject'];
    });
    const speedSumGroup = runDimension.group().reduceSum(d => {
      return +d['jpy'];
    });

    const chart = dc.pieChart('#' + idDiv);
    chart
      .width(this.innerWidth / 3)
      .height(this.innerWidth / 3)
      .innerRadius(100)
      .dimension(runDimension)
      .group(speedSumGroup)
      .legend(dc.legend())
      .renderlet((mChart) => {
        mChart.selectAll('text.pie-slice').text((d) => {
          return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2 * Math.PI) * 100) + '%';
        });
      });
  }

  render_bank_account(idDiv: string, ndx) {
    const runDimension = ndx.dimension(d => {
      return d['key_bank_account'];
    });
    const speedSumGroup = runDimension.group().reduceSum(d => {
      return +d['jpy'];
    });

    const chart = dc.pieChart('#' + idDiv);
    chart
      .width(this.innerWidth / 2.5)
      .height(this.innerWidth / 2.5)
      .slicesCap(4)
      .innerRadius(100)
      .dimension(runDimension)
      .group(speedSumGroup)
      .legend(dc.legend())
      .renderlet((mChart) => {
        mChart.selectAll('text.pie-slice').text((d) => {
          return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2 * Math.PI) * 100) + '%';
        });
      });
  }

}
