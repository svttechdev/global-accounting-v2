import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {addDays, endOfDay, endOfMonth, startOfMonth, subMonths} from 'date-fns';
import * as crossfilter from 'crossfilter';
import * as dc from 'dc';
import * as d3Scale from 'd3-scale';
import * as d3 from 'd3';
import {range} from 'rxjs';

@Component({
  selector: 'app-property-report',
  templateUrl: './property-report.component.html',
  styleUrls: ['./property-report.component.css']
})
export class PropertyReportComponent implements OnInit {
  propertyId: string;
  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mArrayRecords: Array<any>;
  mArrayMajorSubjects: Array<any>;
  mObjectMajorSubjects: any;

  innerWidth: number;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }


    this.route.params.subscribe(params => {

      this.propertyId = params.id;
      this.afService.mSelectedPropertyID = this.propertyId;
      this.fnPromiseGetCategories()
        .then(result => {
        });
    });
  }

  fnPromiseGetCategories() {
    return new Promise(resolve => {
      this.afService.db
        .collection('major_subject')
        .get()
        .then(snapshot => {
          this.mArrayMajorSubjects = new Array<any>();
          this.mObjectMajorSubjects = {};
          const mTmpObj = {};
          snapshot.forEach(majorSubject => {
            const mMajorsubject = majorSubject.data();
            mMajorsubject.id = majorSubject.id;

            mTmpObj[mMajorsubject.id] = mMajorsubject;
            this.mArrayMajorSubjects.push(mMajorsubject);

          });
          this.mObjectMajorSubjects = mTmpObj;
          resolve(true);
        });
    });
  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      this.fnGetRecords(mNewDate);
    }
  }

  fnGetRecords(date: Date) {
    const mStartMonth = +subMonths(startOfMonth(date), 6);
    const mEndMonth = +endOfMonth(date);


    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('record')
      .where('date', '>=', this.afService.fnCreateDate(new Date(mStartMonth)))
      .where('date', '<=', this.afService.fnCreateDate(new Date(mEndMonth)))
      .get()
      .then(snapshot => {

        this.mArrayRecords = new Array<any>();

        snapshot.forEach(record => {
          const mRecord = record.data();

          mRecord.id = record.id;
          const tmpDate = endOfDay(mRecord.date.toDate());

          mRecord.date = +new Date(tmpDate);
          mRecord.month = this.mArrayMonths[tmpDate.getMonth()];

          if (mRecord.expense &&
            mRecord.expense.jpy && mRecord.expense.localCurrency &&
            mRecord.expense.key_major_subject && mRecord.expense.key_minor_subject &&
            mRecord.expense.key_bank_account) {
            mRecord.expense.date = mRecord.date;
            mRecord.expense.month = mRecord.month;
            mRecord.expense.key = '2_Expenses';
            // expense
            const mTmpExp = Object.assign({}, mRecord.expense);
            this.mArrayRecords.push(
              mTmpExp
            );
            // balance
            mRecord.expense.key = '3_Balance';
            mRecord.expense.jpy = mRecord.expense.jpy * -1;
            const mTmpIncome = Object.assign({}, mRecord.expense);
            this.mArrayRecords.push(
              mTmpIncome
            );
          }

          if (mRecord.income &&
            mRecord.income.jpy && mRecord.income.localCurrency &&
            mRecord.income.key_major_subject && mRecord.income.key_minor_subject &&
            mRecord.income.key_bank_account) {
            mRecord.income.date = mRecord.date;
            mRecord.income.month = mRecord.month;
            mRecord.income.key = '1_Income';

            if (mRecord.income.key_major_subject === 'IfyUX5kPssNTVQCgyPzL' ||
              mRecord.income.key_major_subject === '9PnDyidDqDGLnMCENcXy') {
              //
              const mTmpIncome = Object.assign({}, mRecord.income);
              this.mArrayRecords.push(
                mTmpIncome
              );

              mRecord.income.key = '3_Balance';
              const mTmpBalance = Object.assign({}, mRecord.income);
              this.mArrayRecords.push(
                mTmpBalance
              );


            }
          }

        });
        this.fnInitGraphs();
      });
  }


  fnInitGraphs() {
    const ndx = crossfilter(this.mArrayRecords);

    const dateDimension = ndx.dimension((d) => {
      return d.date;
    });
    const maxDate = dateDimension.top(1)[0].date;
    const minDate = dateDimension.bottom(1)[0].date;

    this.render_graph_composite_balance1('balance-chart', ndx, minDate, maxDate);
    this.render_graph_line('hotel-sales-chart', ndx, minDate, maxDate, '9PnDyidDqDGLnMCENcXy');
    this.render_graph_line('restaurant-sales-chart', ndx, minDate, maxDate, 'IfyUX5kPssNTVQCgyPzL');

    dc.renderAll();
  }


  render_graph_line(idDiv: string, ndx, minDate, maxDate, id: string) {

    const numberNightsDimension = ndx.dimension(d => {
      return this.mArrayMonths[new Date(d.date).getMonth()];
    });

    const domainX = d3.scaleBand();

    const lineChart = dc.barChart('#' + idDiv);

    const margins = {top: 100, right: 110, bottom: 50, left: 50};
    const mWidth = this.innerWidth / 1;
    const mHeight = this.innerWidth / 3;

    const mGroup = numberNightsDimension.group().reduceSum(d => {
      if (d.key === '1_Income' &&
        (d.key_major_subject === id)) {
        return d.jpy;
      }
      return 0;
    });

    lineChart
      .width(mWidth)
      .height(mHeight)
      .margins(margins)
      .x(domainX)
      .xUnits(dc.units.ordinal)
      .dimension(numberNightsDimension)
      .group(mGroup);
  }

  render_graph_composite_balance1(idDiv: string, ndx, minDate, maxDate) {

    const margin = {top: 20, right: 20, bottom: 30, left: 100};
    const width = (this.innerWidth * 0.9) - margin.left - margin.right;
    const height = (this.innerWidth * 0.7) - margin.top - margin.bottom;


    const x0 =
      d3Scale.scaleBand()
        .rangeRound([0, width]);
    const x1 = d3.scaleOrdinal();

    const mBalanceDim = ndx.dimension((d) => {
      return d.jpy;
    });

    // const minBal = mBalanceDim.bottom(1)[0].jpy;
    // const maxBal = mBalanceDim.top(1)[0].jpy;

    const y = d3Scale.scaleLinear()
      .range([height, 0]);

    // const color = d3.scaleOrdinal()
    //   .range(['#98abc5', '#8a89a6', '#7b6888', '#6b486b', '#a05d56', '#d0743c', '#ff8c00']);

    const mColors = {
      '1_Income': '#a8c5a5',
      '2_Expenses': '#d88712',
      '3_Balance': '#78b1c0'
    };

    const xAxis = d3.axisBottom(x0)
      .scale(x0);

    const yAxis = d3.axisLeft(y);

    d3.select('#' + idDiv + ' svg').remove();

    const svg = d3.select('#' + idDiv).append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    const stateAgeNames = ndx.dimension((d) => {
      return [d.month, d.key];
    });

    const stateAgeGroup = stateAgeNames.group().reduceSum((d) => {
      return d.jpy;
    });

    const stateAccessor = (d) => {
      return d.key[0];
    };

    const ageNameAccessor = (d) => {
      return d.key[1];
    };

    const populationAccessor = (d) => {
      return d.value;
    };

    const filteredData = stateAgeGroup.all();

    const ageNames = d3.set(filteredData.map(ageNameAccessor)).values();
    const states = d3.set(filteredData.map(stateAccessor)).values();

    const maxPopulation = d3.max(filteredData.map(populationAccessor));
    const minPopulation = d3.min(filteredData.map(populationAccessor));
    console.log('max');
    console.log(maxPopulation);
    console.log('min');
    console.log(minPopulation);

    x0.domain(states);
    x1.domain(ageNames)
      .range([0, x0.range()]);

    y.domain([+minPopulation, +maxPopulation]);

    interface YourDatum {
      key: string;
      // value: {};
      // any proprties related to the value of this element
    }


    const nestedData = d3.nest<YourDatum, number>()
      .key(stateAccessor)
      .key(ageNameAccessor)
      .rollup((d) => {
        return populationAccessor(d[0]);
      })
      .entries(filteredData);

    // svg.append('g')
    //   .attr('class', 'x axis')
    //   .attr('transform', 'translate(0,' + height + ')')
    //   .call(xAxis);

    let xAxisTransform = height;
    if (+minPopulation < 0 && 0 < +maxPopulation) {
      xAxisTransform = height * (+maxPopulation / (+maxPopulation - +minPopulation));
    }

    svg.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '.71em')
      .style('text-anchor', 'end')
      .text('JPY');

    const state = svg.selectAll('.state')
      .data(nestedData)
      .enter().append('g')
      .attr('class', 'g')
      .attr('transform', (d: any) => {
        return 'translate(' + x0(d.key) + ',0)';
      });

    state.selectAll('rect')
      .data((d: any) => {
        return d.values;
      })
      .enter().append('rect')
      .attr('width', d => {
        return x0.bandwidth() / 3.5;
      })
      .attr('x', (d: any) => {
        return ((ageNames.indexOf(d.key) % ageNames.length) * (x0.bandwidth() / 3) );
      })
      .attr('y', (d: any) => {
        return (d.value >= 0 ? y(d.value) : xAxisTransform );
      })
      .attr('height', (d: any) => {
        return Math.abs(xAxisTransform - y(d.value));
      })
      .style('fill', (d: any) => {
        return mColors[d.key];
      });

    state.selectAll('.text')
      .data((d: any) => {
        return d.values;
      })
      .enter()
      .append('text')
      .attr('width', d => {
        return x0.bandwidth() / 3.5;
      })
      .attr('x', (d: any) => {
        return ((ageNames.indexOf(d.key) % ageNames.length) * (x0.bandwidth() / 3) ) + ((x0.bandwidth() / 3) / 4 );
      })
      .attr('y', (d: any) => {
        return (d.value >= 0 ? y(d.value) : xAxisTransform );
      })
      .attr('height', (d: any) => {
        return Math.abs(xAxisTransform - y(d.value));
      })
      .attr('dy', '.75em')
      .text((d: any) => {
        return d.key.split('_')[1] + ': ' + d.value.toLocaleString(
          undefined,
          { minimumFractionDigits: 2 }
        );
      });

    svg.append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(0,' + xAxisTransform + ')') // this line moves x-axis
      .call(xAxis);

    const legend = svg.selectAll('.legend')
      .data(ageNames.slice().reverse())
      .enter().append('g')
      .attr('class', 'legend')
      .attr('transform', (d, i) => 'translate(0,' + i * 20 + ')');


    legend.append('rect')
      .attr('x', width - 18)
      .attr('width', 18)
      .attr('height', 18)
      .style('fill', (d: any) => {
        return mColors[d];
      });

    legend.append('text')
      .attr('x', width - 24)
      .attr('y', 9)
      .attr('dy', '.35em')
      .style('text-anchor', 'end')
      .text((d) => {
        return d.split('_')[1];
      });
  }

  render_graph_composite_balance(idDiv: string, dateDimension, minDate, maxDate) {
    const domainX =
      d3Scale.scaleTime()
        .domain([
            minDate,
            maxDate
          ]
        );

    const compositeChart = dc.compositeChart('#' + idDiv);

    const margins = {top: 100, right: 110, bottom: 50, left: 100};
    const mWidth = this.innerWidth / 1.1;
    const mHeight = this.innerWidth / 3;

    const mTmpArray = new Array();

    // // INCOMES
    const incomes = dateDimension.group().reduce(
      (p, v) => {
        if (v.key === '1_Income' &&
          (v.key_major_subject === 'IfyUX5kPssNTVQCgyPzL' || v.key_major_subject === '9PnDyidDqDGLnMCENcXy')) {
          p.total += v.jpy || 0;
        }
        return p;
      },
      (p, v) => {
        if (v.key === '1_Income' &&
          (v.key_major_subject === 'IfyUX5kPssNTVQCgyPzL' || v.key_major_subject === '9PnDyidDqDGLnMCENcXy')) {
          p.total -= v.jpy || 0;
        }
        return p;
      },
      () => {
        return {total: 0};
      }
    );
    mTmpArray.push(
      dc.lineChart(compositeChart)
        .dimension(dateDimension)
        .colors(d3.schemeSet1[0])
        .group(incomes, 'Hotel + Restaurant')
        .valueAccessor(d => {
          return d.value.total;
        })
    );
    // EXPENSES
    const expenses = dateDimension.group().reduce(
      (p, v) => {
        if (v.key === '2_Expenses') {
          p.total += v.jpy || 0;
        }
        return p;
      },
      (p, v) => {
        if (v.key === '2_Expenses') {
          p.total -= v.jpy || 0;
        }
        return p;
      },
      () => {
        return {total: 0};
      }
    );
    mTmpArray.push(
      dc.lineChart(compositeChart)
        .dimension(dateDimension)
        .colors(d3.schemeSet1[1])
        .group(expenses, 'Expenses')
        .valueAccessor(d => {
          return d.value.total;
        })
    );

    // BALANCE
    const balance = dateDimension.group().reduce(
      (p, v) => {
        if (v.key === '1_Income' &&
          (v.key_major_subject === 'IfyUX5kPssNTVQCgyPzL' || v.key_major_subject === '9PnDyidDqDGLnMCENcXy')) {
          p.incomes += v.jpy || 0;
        }
        if (v.key === '2_Expenses') {
          p.expenses += v.jpy || 0;
        }
        p.total = p.incomes - p.expenses;
        return p;
      },
      (p, v) => {
        if (v.key === '1_Income' &&
          (v.key_major_subject === 'IfyUX5kPssNTVQCgyPzL' || v.key_major_subject === '9PnDyidDqDGLnMCENcXy')) {
          p.incomes -= v.jpy || 0;
        }
        if (v.key === '2_Expenses') {
          p.expenses -= v.jpy || 0;
        }
        p.total = p.incomes - p.expenses;
        return p;
      },
      () => {
        return {total: 0, expenses: 0, incomes: 0};
      }
    );
    mTmpArray.push(
      dc.lineChart(compositeChart)
        .dimension(dateDimension)
        .colors(d3.schemeSet1[2])
        .group(balance, 'Balance')
        .valueAccessor(d => {
          return d.value.total;
        })
    );

    compositeChart
      .width(mWidth)
      .height(mHeight)
      .transitionDuration(1000)
      .margins(margins)
      .dimension(dateDimension)
      .x(domainX)
      .legend(
        dc.legend().x(125).y(20).itemHeight(15).gap(5)
      )
      .renderHorizontalGridLines(true)
      .renderVerticalGridLines(true)
      .compose(mTmpArray)
      .brushOn(false);

  }
}
