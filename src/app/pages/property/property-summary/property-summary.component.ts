import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../../services/authentication.service";
import {ActivatedRoute} from "@angular/router";
import {addDays, endOfMonth, getDaysInMonth, startOfDay, startOfMonth} from "date-fns";

@Component({
  selector: 'app-property-summary',
  templateUrl: './property-summary.component.html',
  styleUrls: ['./property-summary.component.css']
})
export class PropertySummaryComponent implements OnInit {

  propertyId: string;
  mObjCategories: any;
  mArrayDays: Array<any>;
  mArrayIncomeCategories: Array<any>;
  mArrayExpensesCategories: Array<any>;

  mObjectMajorCat: any;
  mObjectMinorCat: any;
  mObjectTotalCategory: any;

  mStartOfMonth: number;
  mEndOfMonth: number;

  mTotal = {
    expenses: 0,
    income: 0
  };

  mArrayMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  mArrayYears: Array<any>;
  mSelectedDate = {
    year: -1,
    month: -1
  };

  mProperty: any;
  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {

    let mStartYear = 2019;
    this.mArrayYears = new Array<any>();
    while (mStartYear <= new Date().getFullYear()) {
      this.mArrayYears.push(mStartYear);
      mStartYear++;
    }


    this.route.params.subscribe(params => {

      this.propertyId = params['id'];
      this.afService.mSelectedPropertyID = this.propertyId;

      this.fnInitRecords(new Date());

      this.afService.db
        .collection('properties')
        .doc(this.propertyId)
        .get()
        .then(snapProperty => {
          this.mProperty = snapProperty.data();
        });

      this.afService.db
        .collection('major_subject')
        .get()
        .then(snapshot => {
          this.mArrayIncomeCategories = new Array<any>();
          this.mArrayExpensesCategories = new Array<any>();

          const tmpEmpty = {};
          tmpEmpty['total'] = 0;

          snapshot.forEach(snap => {
            const mCategory = snap.data();
            mCategory.id = snap.id;
            tmpEmpty [mCategory.id] = mCategory;
            this.afService.db
              .collection('major_subject')
              .doc(mCategory.id)
              .collection('minor_subject')
              .get()
              .then(minorCategoriesSnapshot => {
                if (!tmpEmpty [mCategory.id] ['minor_subject']) {
                  tmpEmpty [mCategory.id] ['minor_subject'] = {};
                }
                tmpEmpty [mCategory.id].minor_categories = [];
                minorCategoriesSnapshot.forEach(minorSnap => {
                  const mMinorCategory = minorSnap.data();
                  mMinorCategory.id = minorSnap.id;
                  if (!tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id]) {
                    tmpEmpty [mCategory.id] ['minor_subject'] [mMinorCategory.id] = mMinorCategory;
                    tmpEmpty [mCategory.id].minor_categories.push(mMinorCategory);
                  }

                });
                if (mCategory.type === 'income') {
                  this.mArrayIncomeCategories.push(tmpEmpty [mCategory.id]);
                } else if (mCategory.type === 'expenses') {
                  this.mArrayExpensesCategories.push(tmpEmpty [mCategory.id]);
                }

              });
          });
          this.mObjCategories = tmpEmpty;
        });
    });
  }

  fnInitRecords(mDate: Date) {
    this.mArrayDays = new Array<any>();

    this.mStartOfMonth = +startOfMonth(mDate);
    this.mEndOfMonth = +endOfMonth(mDate);

    this.fnGetAllMonthDays(new Date(mDate));
    this.mSelectedDate.year = mDate.getFullYear();
    this.mSelectedDate.month = mDate.getMonth();
    this.fnGetRecords();
  }

  fnGetRecords() {

    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('record')
      .where('date', '>=', this.afService.fnCreateDate(new Date(this.mStartOfMonth)))
      .where('date', '<=', this.afService.fnCreateDate(new Date(this.mEndOfMonth)))
      .get()
      .then(snapshot => {

        const mObjRecordsMinor = {};
        const mObjRecordsMajor = {};
        const mObjTotalCategory = {};
        const mTmpDate = +new Date();

        this.mTotal.expenses = 0;
        this.mTotal.income = 0;

        snapshot.forEach(snap => {

          const mRecord = snap.data();
          mRecord.id = snap.id;

          mRecord.date = mRecord.date.toDate();
          const mIndex = +startOfDay(mRecord.date);

          // minor index
          if (!mObjRecordsMinor[mIndex]) {
            mObjRecordsMinor[mIndex] = {};
          }

          if (mRecord.expense &&
            !mRecord.expense.key_major_subject &&
            !mObjRecordsMinor[mIndex]['UNDEFINED']) {
            mObjRecordsMinor[mIndex]['UNDEFINED'] = {};
          }
          if (mRecord.expense &&
            !mRecord.expense.key_major_subject &&
            !mObjRecordsMinor['UNDEFINED']) {
            mObjRecordsMinor['UNDEFINED'] = {};
          }
          if (mRecord.expense &&
            mRecord.expense.key_major_subject &&
            !mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject]) {
            mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject] = {};
          }
          if (mRecord.expense &&
            mRecord.expense.key_major_subject &&
            !mObjRecordsMinor[mRecord.expense.key_major_subject]) {
            mObjRecordsMinor[mRecord.expense.key_major_subject] = {};
          }
          if (mRecord.expense &&
            !mRecord.expense.key_major_subject &&
            !mRecord.expense.key_minor_subject &&
            !mObjRecordsMinor[mIndex]['UNDEFINED']['UNDEFINED']) {
            mRecord.expense.key_major_subject = 'UNDEFINED';
            mRecord.expense.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor[mIndex]['UNDEFINED']['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.expense &&
            !mRecord.expense.key_major_subject &&
            !mRecord.expense.key_minor_subject &&
            !mObjRecordsMinor['UNDEFINED']['UNDEFINED']) {
            mRecord.expense.key_major_subject = 'UNDEFINED';
            mRecord.expense.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor['UNDEFINED']['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.expense &&
            mRecord.expense.key_major_subject &&
            mRecord.expense.key_minor_subject &&
            !mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject]) {
            mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject] = {
              amount: 0
            };
          }
          if (mRecord.expense &&
            mRecord.expense.key_major_subject &&
            mRecord.expense.key_minor_subject &&
            !mObjRecordsMinor[mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject]) {
            mObjRecordsMinor[mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject] = {
              total: 0
            };
          }
          if (mRecord.income &&
            !mRecord.income.key_major_subject &&
            !mObjRecordsMinor[mIndex]['UNDEFINED']) {
            mObjRecordsMinor[mIndex]['UNDEFINED'] = {};
          }
          if (mRecord.income &&
            !mRecord.income.key_major_subject &&
            !mObjRecordsMinor['UNDEFINED']) {
            mObjRecordsMinor['UNDEFINED'] = {};
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            !mObjRecordsMinor[mIndex][mRecord.income.key_major_subject]) {
            mObjRecordsMinor[mIndex][mRecord.income.key_major_subject] = {};
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            !mObjRecordsMinor[mRecord.income.key_major_subject]) {
            mObjRecordsMinor[mRecord.income.key_major_subject] = {};
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            !mRecord.income.key_minor_subject &&
            !mObjRecordsMinor[mIndex][mRecord.income.key_major_subject]['UNDEFINED']) {
            mRecord.income.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor[mIndex][mRecord.income.key_major_subject]['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            !mRecord.income.key_minor_subject &&
            !mObjRecordsMinor[mRecord.income.key_major_subject]['UNDEFINED']) {
            mRecord.income.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor[mRecord.income.key_major_subject]['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            !mRecord.income.key_major_subject &&
            !mRecord.income.key_minor_subject &&
            !mObjRecordsMinor[mIndex]['UNDEFINED']['UNDEFINED']) {
            mRecord.income.key_major_subject = 'UNDEFINED';
            mRecord.income.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor[mIndex]['UNDEFINED']['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            !mRecord.income.key_major_subject &&
            !mRecord.income.key_minor_subject &&
            !mObjRecordsMinor['UNDEFINED']['UNDEFINED']) {
            mRecord.income.key_major_subject = 'UNDEFINED';
            mRecord.income.key_minor_subject = 'UNDEFINED';
            mObjRecordsMinor['UNDEFINED']['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            mRecord.income.key_minor_subject &&
            !mObjRecordsMinor[mIndex][mRecord.income.key_major_subject][mRecord.income.key_minor_subject]) {
            mObjRecordsMinor[mIndex][mRecord.income.key_major_subject][mRecord.income.key_minor_subject] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            mRecord.income.key_minor_subject &&
            !mObjRecordsMinor[mRecord.income.key_major_subject][mRecord.income.key_minor_subject]) {
            mObjRecordsMinor[mRecord.income.key_major_subject][mRecord.income.key_minor_subject] = {
              total: 0
            };
          }

          // major index
          if (!mObjRecordsMajor[mIndex]) {
            mObjRecordsMajor[mIndex] = {};
          }
          if (mRecord.expense &&
            !mRecord.expense.key_major_subject &&
            !mObjRecordsMajor[mIndex]['UNDEFINED']) {
            mRecord.expense.key_major_subject = 'UNDEFINED';
            mObjRecordsMajor[mIndex]['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.expense &&
            mRecord.expense.key_major_subject &&
            !mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject]) {

            mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject] = {
              amount: 0
            };
          }

          if (mRecord.income &&
            !mRecord.income.key_major_subject &&
            !mObjRecordsMajor[mIndex]['UNDEFINED']) {
            mRecord.income.key_major_subject = 'UNDEFINED';
            mObjRecordsMajor[mIndex]['UNDEFINED'] = {
              amount: 0
            };
          }
          if (mRecord.income &&
            mRecord.income.key_major_subject &&
            !mObjRecordsMajor[mIndex][mRecord.income.key_major_subject]) {

            mObjRecordsMajor[mIndex][mRecord.income.key_major_subject] = {
              amount: 0
            };
          }

          if (!mObjRecordsMajor[mIndex]['total']) {
            mObjRecordsMajor[mIndex]['total'] = {
              income: 0,
              expense: 0
            };
          }
          // sum
          if (mRecord.income && mRecord.income.jpy) {

            let mMajorKey = 'UNDEFINED';
            let mMinorKey = 'UNDEFINED';
            if (mRecord.income.key_major_subject) {
              mMajorKey =  mRecord.income.key_major_subject;
            }
            if (mRecord.income.key_minor_subject) {
              mMinorKey =  mRecord.income.key_minor_subject;
            }
            if (!mObjRecordsMinor[mIndex][mMajorKey][mMinorKey]) {
              mObjRecordsMinor[mIndex][mMajorKey][mMinorKey] = {
                amount: 0
              };
            }

            mObjRecordsMinor[mMajorKey][mMinorKey].total += mRecord.income.jpy;
            mObjRecordsMinor[mIndex][mMajorKey][mMinorKey].amount += mRecord.income.jpy;
            mObjRecordsMajor[mIndex][mMajorKey].amount += mRecord.income.jpy;

            if (mRecord.income.key_major_subject) {
              mObjRecordsMajor[mIndex]['total'].income += mRecord.income.jpy;
            }
            if (!mRecord.income.key_major_subject) {
              mObjRecordsMajor[mIndex]['total'].income += mRecord.income.jpy;
            }

            if ( !mObjTotalCategory[mRecord.income.key_major_subject] ) {
              mObjTotalCategory[mRecord.income.key_major_subject] = {
                total: 0,
                post_total: 0,
                pre_total: 0
              };
            }
            mObjTotalCategory[mRecord.income.key_major_subject].total += mRecord.income.jpy || 0;
            if (+new Date(mRecord.date) > mTmpDate) {
              mObjTotalCategory[mRecord.income.key_major_subject].post_total += mRecord.income.jpy || 0;
            } else {
              mObjTotalCategory[mRecord.income.key_major_subject].pre_total += mRecord.income.jpy || 0;
            }
            this.mTotal.income += mRecord.income.jpy;
          }
          if (mRecord.expense && mRecord.expense.jpy) {
            mObjRecordsMinor[mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject].total += mRecord.expense.jpy;
            mObjRecordsMinor[mIndex][mRecord.expense.key_major_subject][mRecord.expense.key_minor_subject].amount += mRecord.expense.jpy;
            mObjRecordsMajor[mIndex][mRecord.expense.key_major_subject].amount += mRecord.expense.jpy;
            if (mRecord.expense.key_major_subject) {
              mObjRecordsMajor[mIndex]['total'].expense += mRecord.expense.jpy;
            }
            if ( !mObjTotalCategory[mRecord.expense.key_major_subject] ) {
              mObjTotalCategory[mRecord.expense.key_major_subject] = {
                total: 0,
                post_total: 0,
                pre_total: 0
              };
            }

            mObjTotalCategory[mRecord.expense.key_major_subject || 'UNDEFINED'].total += mRecord.expense.jpy || 0;
            if (+new Date(mRecord.date) > mTmpDate) {
              mObjTotalCategory[mRecord.expense.key_major_subject || 'UNDEFINED'].post_total += mRecord.expense.jpy || 0;
            } else {
              mObjTotalCategory[mRecord.expense.key_major_subject || 'UNDEFINED'].pre_total += mRecord.expense.jpy || 0;
            }
            this.mTotal.expenses += mRecord.expense.jpy;
          }

        });
        this.mObjectMajorCat = mObjRecordsMajor;
        this.mObjectMinorCat = mObjRecordsMinor;
        this.mObjectTotalCategory = mObjTotalCategory;

      });
  }

  fnGetAllMonthDays(refDate: Date) {
    const tmpObj = {};
    const totalDays = +getDaysInMonth(refDate);
    let keyIndex = +startOfMonth(refDate);

    this.mArrayDays = new Array<any>();

    for (let i = 0; i < totalDays; i++) {
      tmpObj[keyIndex] = {};
      this.mArrayDays.push(keyIndex);
      keyIndex = +addDays(keyIndex, 1);
    }

  }

  fnChangeMonth(mMonth) {
    if (this.mArrayMonths.indexOf(mMonth) >= 0) {

      this.mSelectedDate.month = this.mArrayMonths.indexOf(mMonth);

      const mNewDate = new Date();
      mNewDate.setFullYear(this.mSelectedDate.year);
      mNewDate.setMonth(this.mSelectedDate.month);

      this.fnInitRecords(mNewDate);

    }
  }

}
