import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EAddBankAccountComponent } from './e-add-bank-account.component';

describe('EAddBankAccountComponent', () => {
  let component: EAddBankAccountComponent;
  let fixture: ComponentFixture<EAddBankAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EAddBankAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EAddBankAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
