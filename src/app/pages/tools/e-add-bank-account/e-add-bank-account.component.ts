import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {BankAccount} from '../../../classes/bank-account';
import {Contact} from '../../../classes/contact';

@Component({
  selector: 'app-e-add-bank-account',
  templateUrl: './e-add-bank-account.component.html',
  styleUrls: ['./e-add-bank-account.component.css']
})
export class EAddBankAccountComponent implements OnInit {

  propertyId: string;
  mBankAccount: BankAccount;

  mArrayBankAccount: Array<any>;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = params['key_property'];
      this.afService.mSelectedPropertyID = this.propertyId;
    });
    this.mBankAccount = new BankAccount();
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('bank_account')
      .onSnapshot(snapshot => {

        this.mArrayBankAccount = new Array<any>();

        snapshot.forEach(snap => {
          const mBankAccount = snap.data();
          mBankAccount.id = snap.id;

          this.mArrayBankAccount.push(mBankAccount);

        });
        console.log(this.mArrayBankAccount);

      });
  }

  fnSaveBankAccount() {
    console.log(this.mBankAccount);
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('bank_account')
      .add(
        Object.assign({}, this.mBankAccount)
      )
      .then(docRef => {
        console.log('Document successfully written!');
        this.mBankAccount = new Contact();
      });
  }

  fnDeleteBankAccount(bankAccount) {
    console.log(bankAccount);
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('bank_account')
      .doc(bankAccount.id)
      .delete().then( () => {
      console.log('Document successfully deleted!');
    }).catch( (error) => {
      console.error('Error removing document: ', error);
    });
  }

  fnUpdateBankAccount(banAccountUpdate) {
    this.mBankAccount = Object.assign(this.mBankAccount, banAccountUpdate);
    console.log(this.mBankAccount);
  }

  fnUpdate() {
    this.afService.db
      .collection('properties')
      .doc(this.propertyId)
      .collection('bank_account')
      .doc(this.mBankAccount.id)
      .update(
        Object.assign({}, this.mBankAccount)
      )
      .then( () => {
        console.log('Document successfully updated!');
        this.mBankAccount = new Contact();
      })
      .catch( (error) => {
        console.error('Error updating document: ', error);
      });
  }

}
