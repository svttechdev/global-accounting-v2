import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EAddCategoryComponent } from './e-add-category.component';

describe('EAddCategoryComponent', () => {
  let component: EAddCategoryComponent;
  let fixture: ComponentFixture<EAddCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EAddCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EAddCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
