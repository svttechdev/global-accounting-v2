import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {PropertyRecord} from '../../../classes/property-record';
import {MajorSubject} from '../../../classes/major-subject';
import {ActivatedRoute, Route} from '@angular/router';

@Component({
  selector: 'app-e-add-category',
  templateUrl: './e-add-category.component.html',
  styleUrls: ['./e-add-category.component.css']
})
export class EAddCategoryComponent implements OnInit {

  propertyId: string;
  mMajorSubject: MajorSubject;

  mArrayCategoriesExpenses: Array<any>;
  mArrayCategoriesIncome: Array<any>;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = params['id'];
      this.afService.mSelectedPropertyID = this.propertyId;
    });
    this.mMajorSubject = new MajorSubject();
    this.afService.db
      .collection('major_subject')
      .onSnapshot(snapshot => {

        this.mArrayCategoriesExpenses = new Array<any>();
        this.mArrayCategoriesIncome = new Array<any>();

        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;

          if (mCategory.type === 'expenses') {
            this.mArrayCategoriesExpenses.push(mCategory);
          } else if (mCategory.type === 'income') {
            this.mArrayCategoriesIncome.push(mCategory);
          }



        });
        console.log(this.mArrayCategoriesExpenses);

      });
  }

  fnSaveCategory() {
    console.log(this.mMajorSubject);
    this.afService.db
      .collection('major_subject')
      .add(
        Object.assign({}, this.mMajorSubject)
    )
      .then(docRef => {
        console.log('Document successfully written!');
        this.mMajorSubject = new MajorSubject();
      });
  }

  fnDeleteCategory(category) {
    console.log(category);
    this.afService.db
      .collection('major_subject')
      .doc(category.id)
      .delete().then( () => {
      console.log('Document successfully deleted!');
    }).catch( (error) => {
      console.error('Error removing document: ', error);
    });
  }

  fnUpdateCategory(categoryUpdate) {
    this.mMajorSubject = Object.assign(this.mMajorSubject, categoryUpdate);
    console.log(this.mMajorSubject);
  }

  fnUpdate() {
    this.afService.db
      .collection('major_subject')
      .doc(this.mMajorSubject.id)
      .update(
        Object.assign({}, this.mMajorSubject)
    )
      .then( () => {
        console.log('Document successfully updated!');
        this.mMajorSubject = new MajorSubject();
      })
      .catch( (error) => {
        console.error('Error updating document: ', error);
      });
  }

}
