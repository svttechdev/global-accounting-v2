import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EAddContactComponent } from './e-add-contact.component';

describe('EAddContactComponent', () => {
  let component: EAddContactComponent;
  let fixture: ComponentFixture<EAddContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EAddContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EAddContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
