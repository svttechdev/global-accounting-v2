import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {Contact} from '../../../classes/contact';

@Component({
  selector: 'app-e-add-contact',
  templateUrl: './e-add-contact.component.html',
  styleUrls: ['./e-add-contact.component.css']
})
export class EAddContactComponent implements OnInit {

  propertyId: string;
  mContact: Contact;

  mArrayContact: Array<any>;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = params['id'];
      this.afService.mSelectedPropertyID = this.propertyId;
    });
    this.mContact = new Contact();
    this.afService.db
      .collection('contact')
      .onSnapshot(snapshot => {

        this.mArrayContact = new Array<any>();

        snapshot.forEach(snap => {
          const mContact = snap.data();
          mContact.id = snap.id;

          this.mArrayContact.push(mContact);

        });
        console.log(this.mArrayContact);

      });
  }

  fnSaveContact() {
    console.log(this.mContact);
    this.afService.db
      .collection('contact')
      .add(
        Object.assign({}, this.mContact)
      )
      .then(docRef => {
        console.log('Document successfully written!');
        this.mContact = new Contact();
      });
  }

  fnDeleteContact(contact) {
    console.log(contact);
    this.afService.db
      .collection('contact')
      .doc(contact.id)
      .delete().then( () => {
      console.log('Document successfully deleted!');
    }).catch( (error) => {
      console.error('Error removing document: ', error);
    });
  }

  fnUpdateContact(contactUpdate) {
    this.mContact = Object.assign(this.mContact, contactUpdate);
    console.log(this.mContact);
  }

  fnUpdate() {
    this.afService.db
      .collection('contact')
      .doc(this.mContact.id)
      .update(
        Object.assign({}, this.mContact)
      )
      .then( () => {
        console.log('Document successfully updated!');
        this.mContact = new Contact();
      })
      .catch( (error) => {
        console.error('Error updating document: ', error);
      });
  }

}
