import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EAddSubcategoryComponent } from './e-add-subcategory.component';

describe('EAddSubcategoryComponent', () => {
  let component: EAddSubcategoryComponent;
  let fixture: ComponentFixture<EAddSubcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EAddSubcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EAddSubcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
