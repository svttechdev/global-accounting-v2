import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';
import {MajorSubject} from '../../../classes/major-subject';
import {MinorSubject} from '../../../classes/minor-subject';

@Component({
  selector: 'app-e-add-subcategory',
  templateUrl: './e-add-subcategory.component.html',
  styleUrls: ['./e-add-subcategory.component.css']
})
export class EAddSubcategoryComponent implements OnInit {

  propertyId: string;
  propertyType: string;

  majorSubjectId: string;
  mMajorSubject: string;
  mMinorSubject: MinorSubject;

  mArrayCategories: Array<any>;
  mArraySubcategories: Array<any>;

  constructor(private afService: AuthenticationService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.propertyId = params['id'];
      this.propertyType = params['type'];
      this.afService.mSelectedPropertyID = this.propertyId;
    });
    this.mMajorSubject = '';
    this.mMinorSubject = new MinorSubject();
    this.afService.db
      .collection('major_subject')
      .orderBy('name', 'asc')
      .where('type', '==', this.propertyType)
      .onSnapshot(snapshot => {

        this.mArrayCategories = new Array<any>();

        snapshot.forEach(snap => {
          const mCategory = snap.data();
          mCategory.id = snap.id;

          this.mArrayCategories.push(mCategory);

        });
        console.log(this.mArrayCategories);

      });
  }

  fnEventSelected($event) {
    console.log(this.mMajorSubject);
    this.afService.db
      .collection('major_subject')
      .doc($event)
      .collection('minor_subject')
      .onSnapshot(snapshot => {

        this.mArraySubcategories = new Array<any>();

        snapshot.forEach(snap => {
          const mSubcategory = snap.data();
          mSubcategory.id = snap.id;

          this.mArraySubcategories.push(mSubcategory);

        });
        console.log(this.mArraySubcategories);

      });
    return $event;
  }

  fnSaveSubcategory() {
    console.log(this.mMajorSubject);
    this.afService.db
      .collection('major_subject')
      .doc(this.mMajorSubject)
      .collection('minor_subject')
      .add(
        Object.assign({}, this.mMinorSubject),
      )
      .then(docRef => {
        console.log('Document successfully written!');
        this.mMinorSubject = new MinorSubject();
      });
  }

  fnDeleteSubcategory(subcategory) {
    console.log(subcategory);
    this.afService.db
      .collection('major_subject')
      .doc(this.mMajorSubject)
      .collection('minor_subject')
      .doc(subcategory.id)
      .delete().then(() => {
      console.log('Document successfully deleted!');
    }).catch((error) => {
      console.error('Error removing document: ', error);
    });
  }

  fnUpdateSubcategory(subcategoryUpdate) {
    this.mMinorSubject = Object.assign(this.mMinorSubject, subcategoryUpdate);
    console.log(this.mMinorSubject);
  }

  fnUpdate() {
    this.afService.db
      .collection('major_subject')
      .doc(this.mMajorSubject)
      .collection('minor_subject')
      .doc(this.mMinorSubject.id)
      .update(
        Object.assign({}, this.mMinorSubject)
      )
      .then(() => {
        console.log('Document successfully updated!');
        this.mMinorSubject = new MinorSubject();
      })
      .catch((error) => {
        console.error('Error updating document: ', error);
      });
  }

}
