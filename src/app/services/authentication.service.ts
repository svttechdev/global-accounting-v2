import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import * as firebase from 'firebase';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  user: any;
  uid: string;

  provider: any;
  db: any;
  mAuth: any;
  mSelectedPropertyID: string;

  constructor(private router: Router) {

  }
  fnCheckAuth() {

    firebase.auth().onAuthStateChanged( (user) => {
      // console.log(user);
      if (user) {
        this.uid = user.uid;
        this.user = user;

      } else {
        this.user = undefined;
        this.router.navigate(['./login']);
      }
    });
  }

  fnInitializeFirebase() {
    firebase.initializeApp(environment.firebase);
    this.provider = new firebase.auth.GoogleAuthProvider();
    this.db = firebase.firestore();
    this.mAuth = firebase.auth();
  }

  loginWithGoogle() {
    firebase.auth().signInWithPopup(this.provider).then( (result) => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      // const token = result.credential.accessToken;
      // The signed-in user info.
      // const user = result.user;
      // ...
      this.router.navigate(['./home']);
      console.log(result);
    }).catch((error) => {
      // Handle Errors here.
      // var errorCode = error.code;
      // var errorMessage = error.message;
      // // The email of the user's account used.
      // var email = error.email;
      // // The firebase.auth.AuthCredential type that was used.
      // var credential = error.credential;
      // ...
    });
  }

  /**
   * Logs out the current user
   */
  logout() {
    firebase.auth().signOut().then(out => {
      console.log('OUT');
    });
  }

  fnRedirect(path: string) {
    this.router.navigate(['./' + path]);
  }

  fnCreateDate(mDate) {
    return firebase.firestore.Timestamp.fromDate(new Date(mDate));
  }

}
