import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mProperties: any;
  mPropertyID: any;
  mArrayProperties: Array<any>;

  constructor(public afService: AuthenticationService) {
    this.afService.db
      .collection('properties')
      .orderBy('name', 'asc')
      .onSnapshot(snapshot => {

        const tmpObj = {};
        this.mArrayProperties = new Array<any>();

        snapshot.forEach(snap => {
          const mProperty = snap.data();
          mProperty.id = snap.id;

          tmpObj[mProperty.id] = mProperty;
          this.mArrayProperties.push(mProperty);
        });

        this.mProperties = tmpObj;
      });
  }

  ngOnInit() {
    this.mPropertyID = this.afService.mSelectedPropertyID;
  }

  fnLogOut() {
    this.afService.logout();
  }

}
