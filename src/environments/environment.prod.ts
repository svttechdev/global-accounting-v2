export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyA8kIL4jLRXY_gazKUjOJ08OzxvLYGzQD8',
    authDomain: 'fc-global-accounting.firebaseapp.com',
    databaseURL: 'https://fc-global-accounting.firebaseio.com',
    projectId: 'fc-global-accounting',
    storageBucket: 'fc-global-accounting.appspot.com',
    messagingSenderId: '506705150150',
    appId: '1:506705150150:web:9bd006b057df37154597c7'
  }
};
