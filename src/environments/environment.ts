// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA8kIL4jLRXY_gazKUjOJ08OzxvLYGzQD8',
    authDomain: 'fc-global-accounting.firebaseapp.com',
    databaseURL: 'https://fc-global-accounting.firebaseio.com',
    projectId: 'fc-global-accounting',
    storageBucket: 'fc-global-accounting.appspot.com',
    messagingSenderId: '506705150150',
    appId: '1:506705150150:web:9bd006b057df37154597c7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
